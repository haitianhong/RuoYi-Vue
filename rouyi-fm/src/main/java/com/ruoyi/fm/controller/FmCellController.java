package com.ruoyi.fm.controller;

import java.util.List;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.fm.domain.FmCellSellOrder;
import com.ruoyi.fm.service.IFmCellSellOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.fm.domain.FmCell;
import com.ruoyi.fm.service.IFmCellService;

/**
 * 格位Controller
 *
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/cell")
public class FmCellController extends BaseController
{
    @Autowired
    private IFmCellService fmCellService;
    @Autowired
    private IFmCellSellOrderService fmCellSellOrderService;

    /**
     * 查询格位列表
     */
    @PreAuthorize("@ss.hasPermi('fm:cell:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmCell fmCell)
    {
        startPage();
        List<FmCell> list = fmCellService.selectFmCellList(fmCell);
        return getDataTable(list);
    }

    /**
     * 导出格位列表
     */
    @PreAuthorize("@ss.hasPermi('fm:cell:export')")
    @Log(title = "格位", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmCell fmCell)
    {
        List<FmCell> list = fmCellService.selectFmCellList(fmCell);
        ExcelUtil<FmCell> util = new ExcelUtil<FmCell>(FmCell.class);
        return util.exportExcel(list, "格位数据");
    }

    /**
	 * 获取格位信息
	 */
    @PreAuthorize("@ss.hasPermi('fm:cell:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fmCellService.selectFmCellById(id));
    }

	/**
	 * 获取格位详细信息,包括订单、入土记录等
	 */
	@PreAuthorize("@ss.hasPermi('fm:cell:query')")
	@GetMapping(value = "/detail/{id}")
	public AjaxResult getDetail(@PathVariable("id") String id)
	{
		return AjaxResult.success(fmCellService.selectFmCellDetailById(id));
	}

	/**
	 * 销售格位
	 */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:add')")
	@PutMapping(value = "/sell")
	public AjaxResult sell(@RequestBody FmCellSellOrder fmCellSellOrder)
	{
	    //添加订单
        AjaxResult addResult = toAjax(fmCellSellOrderService.insertFmCellSellOrder(fmCellSellOrder));
	    //更格位状态
        if(((Integer) addResult.get(AjaxResult.CODE_TAG)).intValue()== HttpStatus.SUCCESS)
        {
            return toAjax(fmCellService.setFmCellStatus(fmCellSellOrder.getCellId(),1));
		}
	    return addResult;
	}

    /**
     * 新增格位
     */
    @PreAuthorize("@ss.hasPermi('fm:cell:add')")
    @Log(title = "格位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmCell fmCell)
    {
        return toAjax(fmCellService.insertFmCell(fmCell));
    }

    /**
     * 修改格位
     */
    @PreAuthorize("@ss.hasPermi('fm:cell:edit')")
    @Log(title = "格位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmCell fmCell)
    {
        return toAjax(fmCellService.updateFmCell(fmCell));
    }

    /**
     * 删除格位
     */
    @PreAuthorize("@ss.hasPermi('fm:cell:remove')")
    @Log(title = "格位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fmCellService.deleteFmCellByIds(ids));
    }
}
