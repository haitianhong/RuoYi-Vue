package com.ruoyi.fm.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.fm.domain.*;
import com.ruoyi.fm.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 格位销售Controller
 *
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/cellOrder")
public class FmCellSellOrderController extends BaseController {
    @Autowired
    private IFmCellSellOrderService fmCellSellOrderService;
    @Autowired
    private IFmCellService fmCellService;
    @Autowired
    private IFmCustinfoService fmCustinfoService;
    @Autowired
    private IFmDeadService fmDeadService;
    @Autowired
    private IFmPutInCellService fmPutInCellService;

    /**
     * 查询格位销售列表
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmCellSellOrder fmCellSellOrder) {
        startPage();
        List<FmCellSellOrder> list = fmCellSellOrderService.selectFmCellSellOrderList(fmCellSellOrder);
        return getDataTable(list);
    }

    /**
     * 查询格位销售列表
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:list')")
    @GetMapping("/detail")
    public AjaxResult detail(FmCellSellOrder fmCellSellOrder) {

        FmCellSellOrder fmCellSellOrderDetail = new FmCellSellOrder();
        FmCell fmCell = new FmCell();
        FmCustinfo fmCustinfo = new FmCustinfo();
        List<FmPutInCell> fmPutInCellList = new ArrayList<FmPutInCell>();
        FmDead fmDead = new FmDead();

        //获取订单，有ID根据ID获取订单信息，没有ID根据格位ID获取当前生效订单信息，其余的无效
        if (fmCellSellOrder.getId() != null) {
            fmCellSellOrderDetail = fmCellSellOrderService.selectFmCellSellOrderById(fmCellSellOrder.getId());
        } else if (fmCellSellOrder.getCellId() != null) {
            FmCellSellOrder fmCellSellOrderQuery = new FmCellSellOrder();
            fmCellSellOrderQuery.setCellId(fmCellSellOrder.getCellId());
            List<FmCellSellOrder> cellOrderList = fmCellSellOrderService.selectFmCellSellOrderList(fmCellSellOrderQuery);
            boolean isOrder = false;
            for (int i = 0; i < cellOrderList.size(); i++) {
                fmCellSellOrderDetail = cellOrderList.get(i);
                if (fmCellSellOrderDetail.getStatus().intValue() != 0) {
                    isOrder = true;
                    break;
                }
            }
            if (!isOrder) {
                return AjaxResult.error();
            }
        } else {
            return AjaxResult.error();
        }

        //格位信息
        fmCell = fmCellService.selectFmCellById(fmCellSellOrderDetail.getCellId().toString());
        fmCustinfo = fmCustinfoService.selectFmCustinfoById(fmCellSellOrderDetail.getCustomerId());
        //fmDeadService;
        FmPutInCell fmPutInCellQuery = new FmPutInCell();
        fmPutInCellQuery.setCellOrderId(fmCellSellOrderDetail.getId());
        fmPutInCellList = fmPutInCellService.selectFmPutInCellList(fmPutInCellQuery);
        for (int i = 0; i < fmPutInCellList.size(); i++) {
            fmPutInCellQuery = fmPutInCellList.get(i);
            if (fmPutInCellQuery.getStatus() == 0) {
                fmDead = fmDeadService.selectFmDeadById(fmPutInCellQuery.getDeathId());
                break;
            }
        }

        AjaxResult detailResult = AjaxResult.success();
        detailResult.put("fmCellSellOrderDetail", fmCellSellOrderDetail);
        detailResult.put("fmCell", fmCell);
        detailResult.put("fmCustinfo", fmCustinfo);
        detailResult.put("fmPutInCellList", fmPutInCellList);
        detailResult.put("fmDead", fmDead);
        return detailResult;
    }

    /**
     * 导出格位销售列表
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:export')")
    @Log(title = "格位销售", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmCellSellOrder fmCellSellOrder) {
        List<FmCellSellOrder> list = fmCellSellOrderService.selectFmCellSellOrderList(fmCellSellOrder);
        ExcelUtil<FmCellSellOrder> util = new ExcelUtil<FmCellSellOrder>(FmCellSellOrder.class);
        return util.exportExcel(list, "格位销售数据");
    }

    /**
     * 获取格位销售详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fmCellSellOrderService.selectFmCellSellOrderById(id));
    }

    /**
     * 获取格位销售详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:query')")
    @PostMapping(value = "/endCellOrder/{id}/{cellId}")
    public AjaxResult endCellOrder(@PathVariable("id") Long id, @PathVariable("cellId") Long cellId) {
        fmCellSellOrderService.setFmCellOrderStatus(id, 4l);
        return AjaxResult.success(fmCellService.setFmCellStatus(cellId, 0));
    }

    /**
     * 新增格位销售
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:add')")
    @Log(title = "格位销售", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmCellSellOrder fmCellSellOrder) {
        return toAjax(fmCellSellOrderService.insertFmCellSellOrder(fmCellSellOrder));
    }

    /**
     * 修改格位销售
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:edit')")
    @Log(title = "格位销售", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmCellSellOrder fmCellSellOrder) {
        return toAjax(fmCellSellOrderService.updateFmCellSellOrder(fmCellSellOrder));
    }

    /**
     * 删除格位销售
     */
    @PreAuthorize("@ss.hasPermi('fm:cellOrder:remove')")
    @Log(title = "格位销售", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fmCellSellOrderService.deleteFmCellSellOrderByIds(ids));
    }
}
