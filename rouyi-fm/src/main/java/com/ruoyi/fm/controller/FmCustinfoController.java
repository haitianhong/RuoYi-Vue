package com.ruoyi.fm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.fm.domain.FmCustinfo;
import com.ruoyi.fm.service.IFmCustinfoService;

/**
 * 用户信息Controller
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/custinfo")
public class FmCustinfoController extends BaseController
{
    @Autowired
    private IFmCustinfoService fmCustinfoService;

    /**
     * 查询用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('fm:custinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmCustinfo fmCustinfo)
    {
        startPage();
        List<FmCustinfo> list = fmCustinfoService.selectFmCustinfoList(fmCustinfo);
        return getDataTable(list);
    }

	/**
	 * 出售时查询用户信息列表
	 */
	@GetMapping("/listForSell")
	public TableDataInfo listForSell(String name)
	{
		startPage();
		FmCustinfo custinfo = new FmCustinfo();
		custinfo.setCustName(name);
		List<FmCustinfo> list = fmCustinfoService.selectFmCustinfoList(custinfo);
		return getDataTable(list);
	}

    /**
     * 导出用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('fm:custinfo:export')")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmCustinfo fmCustinfo)
    {
        List<FmCustinfo> list = fmCustinfoService.selectFmCustinfoList(fmCustinfo);
        ExcelUtil<FmCustinfo> util = new ExcelUtil<FmCustinfo>(FmCustinfo.class);
        return util.exportExcel(list, "用户信息数据");
    }

    /**
     * 获取用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:custinfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fmCustinfoService.selectFmCustinfoById(id));
    }

    /**
     * 新增用户信息
     */
    @PreAuthorize("@ss.hasPermi('fm:custinfo:add')")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmCustinfo fmCustinfo)
    {
        return toAjax(fmCustinfoService.insertFmCustinfo(fmCustinfo));
    }

    /**
     * 修改用户信息
     */
    @PreAuthorize("@ss.hasPermi('fm:custinfo:edit')")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmCustinfo fmCustinfo)
    {
        return toAjax(fmCustinfoService.updateFmCustinfo(fmCustinfo));
    }

    /**
     * 删除用户信息
     */
    @PreAuthorize("@ss.hasPermi('fm:custinfo:remove')")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fmCustinfoService.deleteFmCustinfoByIds(ids));
    }
}
