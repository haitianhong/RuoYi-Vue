package com.ruoyi.fm.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.fm.domain.FmDead;
import com.ruoyi.fm.service.IFmDeadService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 逝者信息Controller
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/dead")
public class FmDeadController extends BaseController
{
    @Autowired
    private IFmDeadService fmDeadService;

    /**
     * 查询逝者信息列表
     */
    @PreAuthorize("@ss.hasPermi('fm:dead:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmDead fmDead)
    {
        startPage();
        List<FmDead> list = fmDeadService.selectFmDeadList(fmDead);
        return getDataTable(list);
    }

    /**
     * 导出逝者信息列表
     */
    @PreAuthorize("@ss.hasPermi('fm:dead:export')")
    @Log(title = "逝者信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmDead fmDead)
    {
        List<FmDead> list = fmDeadService.selectFmDeadList(fmDead);
        ExcelUtil<FmDead> util = new ExcelUtil<FmDead>(FmDead.class);
        return util.exportExcel(list, "逝者信息数据");
    }

    /**
     * 获取逝者信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:dead:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fmDeadService.selectFmDeadById(id));
    }

    /**
     * 新增逝者信息
     */
    @PreAuthorize("@ss.hasPermi('fm:dead:add')")
    @Log(title = "逝者信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmDead fmDead)
    {
        return toAjax(fmDeadService.insertFmDead(fmDead));
    }

    /**
     * 修改逝者信息
     */
    @PreAuthorize("@ss.hasPermi('fm:dead:edit')")
    @Log(title = "逝者信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmDead fmDead)
    {
        return toAjax(fmDeadService.updateFmDead(fmDead));
    }

    /**
     * 删除逝者信息
     */
    @PreAuthorize("@ss.hasPermi('fm:dead:remove')")
    @Log(title = "逝者信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fmDeadService.deleteFmDeadByIds(ids));
    }
}
