package com.ruoyi.fm.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.fm.domain.FmGoodSellOrder;
import com.ruoyi.fm.service.IFmGoodSellOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品销售Controller
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/goodOrder")
public class FmGoodSellOrderController extends BaseController
{
    @Autowired
    private IFmGoodSellOrderService fmGoodSellOrderService;

    /**
     * 查询商品销售列表
     */
    @PreAuthorize("@ss.hasPermi('fm:goodOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmGoodSellOrder fmGoodSellOrder)
    {
        startPage();
        List<FmGoodSellOrder> list = fmGoodSellOrderService.selectFmGoodSellOrderList(fmGoodSellOrder);
        return getDataTable(list);
    }

    /**
     * 导出商品销售列表
     */
    @PreAuthorize("@ss.hasPermi('fm:goodOrder:export')")
    @Log(title = "商品销售", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmGoodSellOrder fmGoodSellOrder)
    {
        List<FmGoodSellOrder> list = fmGoodSellOrderService.selectFmGoodSellOrderList(fmGoodSellOrder);
        ExcelUtil<FmGoodSellOrder> util = new ExcelUtil<FmGoodSellOrder>(FmGoodSellOrder.class);
        return util.exportExcel(list, "商品销售数据");
    }

    /**
     * 获取商品销售详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:goodOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fmGoodSellOrderService.selectFmGoodSellOrderById(id));
    }

    /**
     * 新增商品销售
     */
    @PreAuthorize("@ss.hasPermi('fm:goodOrder:add')")
    @Log(title = "商品销售", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmGoodSellOrder fmGoodSellOrder)
    {
        return toAjax(fmGoodSellOrderService.insertFmGoodSellOrder(fmGoodSellOrder));
    }

    /**
     * 修改商品销售
     */
    @PreAuthorize("@ss.hasPermi('fm:goodOrder:edit')")
    @Log(title = "商品销售", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmGoodSellOrder fmGoodSellOrder)
    {
        return toAjax(fmGoodSellOrderService.updateFmGoodSellOrder(fmGoodSellOrder));
    }

    /**
     * 删除商品销售
     */
    @PreAuthorize("@ss.hasPermi('fm:goodOrder:remove')")
    @Log(title = "商品销售", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fmGoodSellOrderService.deleteFmGoodSellOrderByIds(ids));
    }
}
