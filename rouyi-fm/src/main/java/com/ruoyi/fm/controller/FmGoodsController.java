package com.ruoyi.fm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.fm.domain.FmGoods;
import com.ruoyi.fm.service.IFmGoodsService;

/**
 * 商品Controller
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/goods")
public class FmGoodsController extends BaseController
{
    @Autowired
    private IFmGoodsService fmGoodsService;

    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('fm:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmGoods fmGoods)
    {
        startPage();
        List<FmGoods> list = fmGoodsService.selectFmGoodsList(fmGoods);
        return getDataTable(list);
    }

	/**
	 * 查询商品列表
	 */
	// @PreAuthorize("@ss.hasPermi('fm:goods:list')")
	@GetMapping("/listByName")
	public TableDataInfo listByName(String name)
	{
		FmGoods params = new FmGoods();
		params.setName(name);
		List<FmGoods> list = fmGoodsService.selectFmGoodsList(params);
		return getDataTable(list);
	}

    /**
     * 导出商品列表
     */
    @PreAuthorize("@ss.hasPermi('fm:goods:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmGoods fmGoods)
    {
        List<FmGoods> list = fmGoodsService.selectFmGoodsList(fmGoods);
        ExcelUtil<FmGoods> util = new ExcelUtil<FmGoods>(FmGoods.class);
        return util.exportExcel(list, "商品数据");
    }

    /**
     * 获取商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fmGoodsService.selectFmGoodsById(id));
    }

    /**
     * 新增商品
     */
    @PreAuthorize("@ss.hasPermi('fm:goods:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmGoods fmGoods)
    {
        return toAjax(fmGoodsService.insertFmGoods(fmGoods));
    }

    /**
     * 修改商品
     */
    @PreAuthorize("@ss.hasPermi('fm:goods:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmGoods fmGoods)
    {
        return toAjax(fmGoodsService.updateFmGoods(fmGoods));
    }

    /**
     * 删除商品
     */
    @PreAuthorize("@ss.hasPermi('fm:goods:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fmGoodsService.deleteFmGoodsByIds(ids));
    }
}
