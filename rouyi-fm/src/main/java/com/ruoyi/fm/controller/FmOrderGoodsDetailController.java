package com.ruoyi.fm.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.fm.domain.FmOrderGoodsDetail;
import com.ruoyi.fm.service.IFmOrderGoodsDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单商品详情Controller
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/detail")
public class FmOrderGoodsDetailController extends BaseController
{
    @Autowired
    private IFmOrderGoodsDetailService fmOrderGoodsDetailService;

    /**
     * 查询订单商品详情列表
     */
    @PreAuthorize("@ss.hasPermi('fm:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        startPage();
        List<FmOrderGoodsDetail> list = fmOrderGoodsDetailService.selectFmOrderGoodsDetailList(fmOrderGoodsDetail);
        return getDataTable(list);
    }

    /**
     * 导出订单商品详情列表
     */
    @PreAuthorize("@ss.hasPermi('fm:detail:export')")
    @Log(title = "订单商品详情", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        List<FmOrderGoodsDetail> list = fmOrderGoodsDetailService.selectFmOrderGoodsDetailList(fmOrderGoodsDetail);
        ExcelUtil<FmOrderGoodsDetail> util = new ExcelUtil<FmOrderGoodsDetail>(FmOrderGoodsDetail.class);
        return util.exportExcel(list, "订单商品详情数据");
    }

    /**
     * 获取订单商品详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fmOrderGoodsDetailService.selectFmOrderGoodsDetailById(id));
    }

    /**
     * 新增订单商品详情
     */
    @PreAuthorize("@ss.hasPermi('fm:detail:add')")
    @Log(title = "订单商品详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        return toAjax(fmOrderGoodsDetailService.insertFmOrderGoodsDetail(fmOrderGoodsDetail));
    }

    /**
     * 修改订单商品详情
     */
    @PreAuthorize("@ss.hasPermi('fm:detail:edit')")
    @Log(title = "订单商品详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        return toAjax(fmOrderGoodsDetailService.updateFmOrderGoodsDetail(fmOrderGoodsDetail));
    }

    /**
     * 删除订单商品详情
     */
    @PreAuthorize("@ss.hasPermi('fm:detail:remove')")
    @Log(title = "订单商品详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fmOrderGoodsDetailService.deleteFmOrderGoodsDetailByIds(ids));
    }
}
