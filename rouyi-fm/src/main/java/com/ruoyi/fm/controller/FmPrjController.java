package com.ruoyi.fm.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.fm.domain.FmPrj;
import com.ruoyi.fm.service.IFmPrjService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工程管理Controller
 *
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/prj")
public class FmPrjController extends BaseController
{
    @Autowired
    private IFmPrjService fmPrjService;

    /**
     * 查询工程管理列表
     */
    @PreAuthorize("@ss.hasPermi('fm:prj:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmPrj fmPrj)
    {
        startPage();
        List<FmPrj> list = fmPrjService.selectFmPrjList(fmPrj);
        return getDataTable(list);
    }

    /**
     * 导出工程管理列表
     */
    @PreAuthorize("@ss.hasPermi('fm:prj:export')")
    @Log(title = "工程管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmPrj fmPrj)
    {
        List<FmPrj> list = fmPrjService.selectFmPrjList(fmPrj);
        ExcelUtil<FmPrj> util = new ExcelUtil<FmPrj>(FmPrj.class);
        return util.exportExcel(list, "工程管理数据");
    }

    /**
     * 获取工程管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:prj:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fmPrjService.selectFmPrjById(id));
    }

    /**
     * 新增工程管理
     */
    @PreAuthorize("@ss.hasPermi('fm:prj:add')")
    @Log(title = "工程管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmPrj fmPrj)
    {
        fmPrj.setLoginId(getUserId());
        return toAjax(fmPrjService.insertFmPrj(fmPrj));
    }

    /**
     * 修改工程管理
     */
    @PreAuthorize("@ss.hasPermi('fm:prj:edit')")
    @Log(title = "工程管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmPrj fmPrj)
    {
        return toAjax(fmPrjService.updateFmPrj(fmPrj));
    }

    /**
     * 删除工程管理
     */
    @PreAuthorize("@ss.hasPermi('fm:prj:remove')")
    @Log(title = "工程管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fmPrjService.deleteFmPrjByIds(ids));
    }
}
