package com.ruoyi.fm.controller;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.fm.service.IFmCellSellOrderService;
import com.ruoyi.fm.service.IFmCellService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.fm.domain.FmPutInCell;
import com.ruoyi.fm.service.IFmPutInCellService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 出入土订单Controller
 *
 * @author ruoyi
 * @date 2021-09-24
 */
@RestController
@RequestMapping("/fm/inOutOrder")
public class FmPutInCellController extends BaseController {
    @Autowired
    private IFmPutInCellService fmPutInCellService;
    @Autowired
    private IFmCellService fmCellService;
    @Autowired
    private IFmCellSellOrderService fmCellSellOrderService;

    /**
     * 查询出入土订单列表
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(FmPutInCell fmPutInCell) {
        startPage();
        List<FmPutInCell> list = fmPutInCellService.selectFmPutInCellList(fmPutInCell);
        return getDataTable(list);
    }

    /**
     * 导出出入土订单列表
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:export')")
    @Log(title = "出入土订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FmPutInCell fmPutInCell) {
        List<FmPutInCell> list = fmPutInCellService.selectFmPutInCellList(fmPutInCell);
        ExcelUtil<FmPutInCell> util = new ExcelUtil<FmPutInCell>(FmPutInCell.class);
        return util.exportExcel(list, "出入土订单数据");
    }

    /**
     * 获取出入土订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(fmPutInCellService.selectFmPutInCellById(id));
    }

    /**
     * 新增出入土订单
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:add')")
    @Log(title = "入土订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FmPutInCell fmPutInCell) {
        fmPutInCell.setLoginId(getUserId());
        fmPutInCell.setTime(new Date());
        AjaxResult ajaxResult = toAjax(fmPutInCellService.insertFmPutInCell(fmPutInCell));
        //成功，修改状态
        if(Integer.parseInt(ajaxResult.get(AjaxResult.CODE_TAG).toString()) == HttpStatus.SUCCESS)
        {
            //2 已经迁入
            fmCellService.setFmCellStatus(fmPutInCell.getCellId(),2);
            //2 已经迁入
            fmCellSellOrderService.setFmCellOrderStatus(fmPutInCell.getCellOrderId(),2l);
        }
        return ajaxResult ;
    }

    /**
     * 新增出土订单
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:add')")
    @Log(title = "出土订单", businessType = BusinessType.INSERT)
    @PostMapping(value = "/OutOrder")
    public AjaxResult addOutOrder(@RequestBody FmPutInCell fmPutInCell) {
        //迁入订单的ID
        Long cellInId = fmPutInCell.getId();

        fmPutInCell.setId(null);
        fmPutInCell.setLoginId(getUserId());
        fmPutInCell.setTime(new Date());
        AjaxResult ajaxResult = toAjax(fmPutInCellService.insertFmPutInCell(fmPutInCell));

        //迁出成功，修改状态
        if(Integer.parseInt(ajaxResult.get(AjaxResult.CODE_TAG).toString()) == HttpStatus.SUCCESS)
        {
            FmPutInCell fmPutInCellTemp = new FmPutInCell();
            fmPutInCellTemp.setId(cellInId);
            fmPutInCellTemp.setStatus(4l);
            fmPutInCellService.updateFmPutInCell(fmPutInCellTemp);
            fmCellService.setFmCellStatus(fmPutInCell.getCellId(),1);
            fmCellSellOrderService.setFmCellOrderStatus(fmPutInCell.getCellOrderId(),1l);
        }
        return ajaxResult ;
    }

    /**
     * 修改出入土订单
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:edit')")
    @Log(title = "出入土订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FmPutInCell fmPutInCell) {
        return toAjax(fmPutInCellService.updateFmPutInCell(fmPutInCell));
    }

    /**
     * 删除出入土订单
     */
    @PreAuthorize("@ss.hasPermi('fm:inOutOrder:remove')")
    @Log(title = "出入土订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fmPutInCellService.deleteFmPutInCellByIds(ids));
    }
}
