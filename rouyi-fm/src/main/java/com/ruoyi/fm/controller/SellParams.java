package com.ruoyi.fm.controller;

/**
* todo:在此加入类的职责描述、实现原理等。
*
* @author cailx
* @date 2021年10月9日
*/
public class SellParams
{
	private Long cellId;

	private String cellName;

	public String getCellName()
	{
		return cellName;
	}

	public void setCellName(String cellName)
	{
		this.cellName = cellName;
	}

	public Long getCellId()
	{
		return cellId;
	}

	public void setCellId(Long cellId)
	{
		this.cellId = cellId;
	}

	private Long salesId;
	private String salesName;
	private String location;
	private Long price;
	private Long prePay;
	private Long userId;
	private String remark;

	public Long getSalesId()
	{
		return salesId;
	}

	public void setSalesId(Long salesId)
	{
		this.salesId = salesId;
	}

	public String getSalesName()
	{
		return salesName;
	}

	public void setSalesName(String salesName)
	{
		this.salesName = salesName;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public Long getPrice()
	{
		return price;
	}

	public void setPrice(Long price)
	{
		this.price = price;
	}

	public Long getPrePay()
	{
		return prePay;
	}

	public void setPrePay(Long prePay)
	{
		this.prePay = prePay;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

}
