package com.ruoyi.fm.domain;

/**
* todo:在此加入类的职责描述、实现原理等。
*
* @author cailx
* @date 2021年10月7日
*/
public class FmCellDetail
{
	private FmCell cell;
	private FmCellSellOrder order;
	private FmPutInCell putInCell;
	private FmCustinfo cust;

	public FmCell getCell()
	{
		return cell;
	}

	public void setCell(FmCell cell)
	{
		this.cell = cell;
	}

	public FmCellSellOrder getOrder()
	{
		return order;
	}

	public void setOrder(FmCellSellOrder order)
	{
		this.order = order;
	}

	public FmPutInCell getPutInCell()
	{
		return putInCell;
	}

	public void setPutInCell(FmPutInCell putInCell)
	{
		this.putInCell = putInCell;
	}

	public FmCustinfo getCust()
	{
		return cust;
	}

	public void setCust(FmCustinfo cust)
	{
		this.cust = cust;
	}

}
