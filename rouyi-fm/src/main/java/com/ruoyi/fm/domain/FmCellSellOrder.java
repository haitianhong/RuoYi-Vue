package com.ruoyi.fm.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 格位销售对象 fm_cell_sell_order
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmCellSellOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 创建人 */
    private Long loginId;

    /** 用户ID */
	private Long customerId;

    /** 销售人员ID */
    private Long salemanId;

    /** 销售人员 */
    @Excel(name = "销售人员")
    private String salemanName;

    /** 用户 */
    @Excel(name = "用户")
    private String customerName;

    /** 金额 */
    @Excel(name = "金额")
    private Long prize;

    /** 格位名称 */
    @Excel(name = "格位名称")
    private String cellName;

    /** 预付金额 */
    private Long prePay;

    /** 订单编号 */
    private Long cellId;

    /** 起始时间 */
    private Date startTime;

    /** 结束时间 */
    private Date endTime;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLoginId(Long loginId) 
    {
        this.loginId = loginId;
    }

    public Long getLoginId() 
    {
        return loginId;
    }

	public void setCustomerId(Long customerId)
    {
        this.customerId = customerId;
    }

	public Long getCustomerId()
    {
        return customerId;
    }
    public void setSalemanId(Long salemanId) 
    {
        this.salemanId = salemanId;
    }

    public Long getSalemanId() 
    {
        return salemanId;
    }
    public void setSalemanName(String salemanName) 
    {
        this.salemanName = salemanName;
    }

    public String getSalemanName() 
    {
        return salemanName;
    }
    public void setCustomerName(String customerName) 
    {
        this.customerName = customerName;
    }

    public String getCustomerName() 
    {
        return customerName;
    }
    public void setPrize(Long prize) 
    {
        this.prize = prize;
    }

    public Long getPrize() 
    {
        return prize;
    }
    public void setCellName(String cellName) 
    {
        this.cellName = cellName;
    }

    public String getCellName() 
    {
        return cellName;
    }
    public void setPrePay(Long prePay) 
    {
        this.prePay = prePay;
    }

    public Long getPrePay() 
    {
        return prePay;
    }
    public void setCellId(Long cellId) 
    {
        this.cellId = cellId;
    }

    public Long getCellId() 
    {
        return cellId;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("loginId", getLoginId())
            .append("customerId", getCustomerId())
            .append("salemanId", getSalemanId())
            .append("salemanName", getSalemanName())
            .append("customerName", getCustomerName())
            .append("createTime", getCreateTime())
            .append("prize", getPrize())
            .append("cellName", getCellName())
            .append("prePay", getPrePay())
            .append("cellId", getCellId())
            .append("remark", getRemark())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("status", getStatus())
            .toString();
    }
}
