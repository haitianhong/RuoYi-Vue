package com.ruoyi.fm.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户信息对象 fm_custinfo
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmCustinfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String custName;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 身份证 */
    @Excel(name = "身份证")
    private String number;

    /** 手机号 */
    @Excel(name = "手机号")
    private String photone;

    /** 地址 */
    @Excel(name = "地址")
    private String addr;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setPhotone(String photone) 
    {
        this.photone = photone;
    }

    public String getPhotone() 
    {
        return photone;
    }
    public void setAddr(String addr) 
    {
        this.addr = addr;
    }

    public String getAddr() 
    {
        return addr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("custName", getCustName())
            .append("sex", getSex())
            .append("number", getNumber())
            .append("photone", getPhotone())
            .append("addr", getAddr())
            .toString();
    }
}
