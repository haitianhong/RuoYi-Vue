package com.ruoyi.fm.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 逝者信息对象 fm_dead
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmDead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 逝者姓名 */
    @Excel(name = "逝者姓名")
    private String deceased;

    /** 性别 */
    @Excel(name = "性别")
    private Long sex;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String number;

    /** 逝世日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "逝世日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deceasedDate;

    /** 备注 */
    @Excel(name = "备注")
    private String memo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeceased(String deceased) 
    {
        this.deceased = deceased;
    }

    public String getDeceased() 
    {
        return deceased;
    }
    public void setSex(Long sex) 
    {
        this.sex = sex;
    }

    public Long getSex() 
    {
        return sex;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setDeceasedDate(Date deceasedDate) 
    {
        this.deceasedDate = deceasedDate;
    }

    public Date getDeceasedDate() 
    {
        return deceasedDate;
    }
    public void setMemo(String memo) 
    {
        this.memo = memo;
    }

    public String getMemo() 
    {
        return memo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deceased", getDeceased())
            .append("sex", getSex())
            .append("number", getNumber())
            .append("deceasedDate", getDeceasedDate())
            .append("memo", getMemo())
            .toString();
    }
}
