package com.ruoyi.fm.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品销售对象 fm_good_sell_order
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmGoodSellOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 创建人 */
    private Long loginId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String serial;

    /** 金额 */
    @Excel(name = "金额")
    private String money;

    /** 状态 */
    private Long status;

    /** 订单商品详情信息 */
    private List<FmOrderGoodsDetail> fmOrderGoodsDetailList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLoginId(Long loginId) 
    {
        this.loginId = loginId;
    }

    public Long getLoginId() 
    {
        return loginId;
    }
    public void setSerial(String serial) 
    {
        this.serial = serial;
    }

    public String getSerial() 
    {
        return serial;
    }
    public void setMoney(String money) 
    {
        this.money = money;
    }

    public String getMoney() 
    {
        return money;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    public List<FmOrderGoodsDetail> getFmOrderGoodsDetailList()
    {
        return fmOrderGoodsDetailList;
    }

    public void setFmOrderGoodsDetailList(List<FmOrderGoodsDetail> fmOrderGoodsDetailList)
    {
        this.fmOrderGoodsDetailList = fmOrderGoodsDetailList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("loginId", getLoginId())
            .append("createTime", getCreateTime())
            .append("serial", getSerial())
            .append("money", getMoney())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("fmOrderGoodsDetailList", getFmOrderGoodsDetailList())
            .toString();
    }
}
