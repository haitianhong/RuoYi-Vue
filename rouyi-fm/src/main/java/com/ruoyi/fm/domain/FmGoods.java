package com.ruoyi.fm.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品对象 fm_goods
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 商品类型ID */
    @Excel(name = "商品类型ID")
    private Long goodType;

    /** 商品单价 */
    @Excel(name = "商品单价")
    private BigDecimal price;

    /** 数量 */
    private Long num;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGoodType(Long goodType) 
    {
        this.goodType = goodType;
    }

    public Long getGoodType() 
    {
        return goodType;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("goodType", getGoodType())
            .append("price", getPrice())
            .append("num", getNum())
            .append("remark", getRemark())
            .toString();
    }
}
