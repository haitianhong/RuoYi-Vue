package com.ruoyi.fm.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单商品详情对象 fm_order_goods_detail
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmOrderGoodsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /**  商品名称 */
    @Excel(name = " 商品名称")
    private String name;

    /** 单价 */
    @Excel(name = "单价")
    private Long prize;

    /** 数量 */
    @Excel(name = "数量")
    private Long num;

    /** 备注 */
    @Excel(name = "备注")
    private String notice;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrize(Long prize) 
    {
        this.prize = prize;
    }

    public Long getPrize() 
    {
        return prize;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setNotice(String notice) 
    {
        this.notice = notice;
    }

    public String getNotice() 
    {
        return notice;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("prize", getPrize())
            .append("num", getNum())
            .append("notice", getNotice())
            .append("orderId", getOrderId())
            .toString();
    }
}
