package com.ruoyi.fm.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 出入土订单对象 fm_put_in_cell
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public class FmPutInCell extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 登录id */
    private Long loginId;

    /** 办理人姓名 */
    @Excel(name = "办理人姓名")
    private String operatorName;

    /** 办理人身份证 */
    @Excel(name = "办理人身份证")
    private String operatorIdentification;

    /** 办理人手机号码 */
    @Excel(name = "办理人手机号码")
    private String operatorPhone;

    /** 墓园ID */
    private Long cellId;

    /** $column.columnComment */
    @Excel(name = "办理人手机号码")
    private String cellName;

    /** 逝者ID */
    private Long deathId;

    /** 逝者 */
    @Excel(name = "逝者")
    private String deathName;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 操作类型 */
    @Excel(name = "操作类型")
    private Long opType;

    /** $column.columnComment */
    @Excel(name = "操作类型")
    private Long cellOrderId;

    /** 状态 */
    private Long status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLoginId(Long loginId) 
    {
        this.loginId = loginId;
    }

    public Long getLoginId() 
    {
        return loginId;
    }
    public void setOperatorName(String operatorName) 
    {
        this.operatorName = operatorName;
    }

    public String getOperatorName() 
    {
        return operatorName;
    }
    public void setOperatorIdentification(String operatorIdentification) 
    {
        this.operatorIdentification = operatorIdentification;
    }

    public String getOperatorIdentification() 
    {
        return operatorIdentification;
    }
    public void setOperatorPhone(String operatorPhone) 
    {
        this.operatorPhone = operatorPhone;
    }

    public String getOperatorPhone() 
    {
        return operatorPhone;
    }
    public void setCellId(Long cellId) 
    {
        this.cellId = cellId;
    }

    public Long getCellId() 
    {
        return cellId;
    }
    public void setCellName(String cellName) 
    {
        this.cellName = cellName;
    }

    public String getCellName() 
    {
        return cellName;
    }
    public void setDeathId(Long deathId) 
    {
        this.deathId = deathId;
    }

    public Long getDeathId() 
    {
        return deathId;
    }
    public void setDeathName(String deathName) 
    {
        this.deathName = deathName;
    }

    public String getDeathName() 
    {
        return deathName;
    }
    public void setTime(Date time) 
    {
        this.time = time;
    }

    public Date getTime() 
    {
        return time;
    }
    public void setOpType(Long opType) 
    {
        this.opType = opType;
    }

    public Long getOpType() 
    {
        return opType;
    }
    public void setCellOrderId(Long cellOrderId) 
    {
        this.cellOrderId = cellOrderId;
    }

    public Long getCellOrderId() 
    {
        return cellOrderId;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("loginId", getLoginId())
            .append("operatorName", getOperatorName())
            .append("operatorIdentification", getOperatorIdentification())
            .append("operatorPhone", getOperatorPhone())
            .append("cellId", getCellId())
            .append("cellName", getCellName())
            .append("deathId", getDeathId())
            .append("deathName", getDeathName())
            .append("time", getTime())
            .append("opType", getOpType())
            .append("cellOrderId", getCellOrderId())
            .append("status", getStatus())
            .toString();
    }
}
