package com.ruoyi.fm.mapper;

import java.util.List;

import com.ruoyi.fm.domain.FmCell;

/**
 * 格位Mapper接口
 *
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmCellMapper
{
    /**
     * 查询格位
     *
     * @param id 格位主键
     * @return 格位
     */
    public FmCell selectFmCellById(String id);

    /**
     * 查询格位列表
     *
     * @param fmCell 格位
     * @return 格位集合
     */
    public List<FmCell> selectFmCellList(FmCell fmCell);

    /**
     * 新增格位
     *
     * @param fmCell 格位
     * @return 结果
     */
    public int insertFmCell(FmCell fmCell);

    /**
     * 修改格位
     *
     * @param fmCell 格位
     * @return 结果
     */
    public int updateFmCell(FmCell fmCell);

	/**
	 * 出售格位
	 *
	 * @param id 格位主键
	 * @return 结果
	 */
	public int sellFmCell(Long id);

    /**
     * 删除格位
     *
     * @param id 格位主键
     * @return 结果
     */
    public int deleteFmCellById(String id);

    /**
     * 批量删除格位
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmCellByIds(String[] ids);
}
