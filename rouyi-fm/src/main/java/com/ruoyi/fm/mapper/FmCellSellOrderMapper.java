package com.ruoyi.fm.mapper;

import java.util.List;

import com.ruoyi.fm.domain.FmCellSellOrder;

/**
 * 格位销售Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmCellSellOrderMapper 
{
    /**
     * 查询格位销售
     * 
     * @param id 格位销售主键
     * @return 格位销售
     */
    public FmCellSellOrder selectFmCellSellOrderById(Long id);

	/**
	 * 查询格位销售
	 * 
	 * @param id 格位销售主键
	 * @return 格位销售
	 */
	public FmCellSellOrder selectActiveFmCellSellOrder(Long cellId);

    /**
     * 查询格位销售列表
     * 
     * @param fmCellSellOrder 格位销售
     * @return 格位销售集合
     */
    public List<FmCellSellOrder> selectFmCellSellOrderList(FmCellSellOrder fmCellSellOrder);

    /**
     * 新增格位销售
     * 
     * @param fmCellSellOrder 格位销售
     * @return 结果
     */
    public int insertFmCellSellOrder(FmCellSellOrder fmCellSellOrder);

    /**
     * 修改格位销售
     * 
     * @param fmCellSellOrder 格位销售
     * @return 结果
     */
    public int updateFmCellSellOrder(FmCellSellOrder fmCellSellOrder);

    /**
     * 删除格位销售
     * 
     * @param id 格位销售主键
     * @return 结果
     */
    public int deleteFmCellSellOrderById(Long id);

    /**
     * 批量删除格位销售
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmCellSellOrderByIds(Long[] ids);
}
