package com.ruoyi.fm.mapper;

import java.util.List;
import com.ruoyi.fm.domain.FmCustinfo;

/**
 * 用户信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmCustinfoMapper 
{
    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    public FmCustinfo selectFmCustinfoById(Long id);

    /**
     * 查询用户信息列表
     * 
     * @param fmCustinfo 用户信息
     * @return 用户信息集合
     */
    public List<FmCustinfo> selectFmCustinfoList(FmCustinfo fmCustinfo);

    /**
     * 新增用户信息
     * 
     * @param fmCustinfo 用户信息
     * @return 结果
     */
    public int insertFmCustinfo(FmCustinfo fmCustinfo);

    /**
     * 修改用户信息
     * 
     * @param fmCustinfo 用户信息
     * @return 结果
     */
    public int updateFmCustinfo(FmCustinfo fmCustinfo);

    /**
     * 删除用户信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    public int deleteFmCustinfoById(Long id);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmCustinfoByIds(Long[] ids);
}
