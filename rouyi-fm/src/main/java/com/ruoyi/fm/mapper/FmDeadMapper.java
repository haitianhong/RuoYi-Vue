package com.ruoyi.fm.mapper;

import java.util.List;
import com.ruoyi.fm.domain.FmDead;

/**
 * 逝者信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmDeadMapper 
{
    /**
     * 查询逝者信息
     * 
     * @param id 逝者信息主键
     * @return 逝者信息
     */
    public FmDead selectFmDeadById(Long id);

    /**
     * 查询逝者信息列表
     * 
     * @param fmDead 逝者信息
     * @return 逝者信息集合
     */
    public List<FmDead> selectFmDeadList(FmDead fmDead);

    /**
     * 新增逝者信息
     * 
     * @param fmDead 逝者信息
     * @return 结果
     */
    public int insertFmDead(FmDead fmDead);

    /**
     * 修改逝者信息
     * 
     * @param fmDead 逝者信息
     * @return 结果
     */
    public int updateFmDead(FmDead fmDead);

    /**
     * 删除逝者信息
     * 
     * @param id 逝者信息主键
     * @return 结果
     */
    public int deleteFmDeadById(Long id);

    /**
     * 批量删除逝者信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmDeadByIds(Long[] ids);
}
