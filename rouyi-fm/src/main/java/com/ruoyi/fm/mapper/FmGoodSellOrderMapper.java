package com.ruoyi.fm.mapper;

import java.util.List;
import com.ruoyi.fm.domain.FmGoodSellOrder;
import com.ruoyi.fm.domain.FmOrderGoodsDetail;

/**
 * 商品销售Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmGoodSellOrderMapper 
{
    /**
     * 查询商品销售
     * 
     * @param id 商品销售主键
     * @return 商品销售
     */
    public FmGoodSellOrder selectFmGoodSellOrderById(Long id);

    /**
     * 查询商品销售列表
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 商品销售集合
     */
    public List<FmGoodSellOrder> selectFmGoodSellOrderList(FmGoodSellOrder fmGoodSellOrder);

    /**
     * 新增商品销售
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 结果
     */
    public int insertFmGoodSellOrder(FmGoodSellOrder fmGoodSellOrder);

    /**
     * 修改商品销售
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 结果
     */
    public int updateFmGoodSellOrder(FmGoodSellOrder fmGoodSellOrder);

    /**
     * 删除商品销售
     * 
     * @param id 商品销售主键
     * @return 结果
     */
    public int deleteFmGoodSellOrderById(Long id);

    /**
     * 批量删除商品销售
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmGoodSellOrderByIds(Long[] ids);

    /**
     * 批量删除订单商品详情
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmOrderGoodsDetailByIds(Long[] ids);
    
    /**
     * 批量新增订单商品详情
     * 
     * @param fmOrderGoodsDetailList 订单商品详情列表
     * @return 结果
     */
    public int batchFmOrderGoodsDetail(List<FmOrderGoodsDetail> fmOrderGoodsDetailList);
    

    /**
     * 通过商品销售主键删除订单商品详情信息
     * 
     * @param id 商品销售ID
     * @return 结果
     */
    public int deleteFmOrderGoodsDetailByOrderId(Long id);
}
