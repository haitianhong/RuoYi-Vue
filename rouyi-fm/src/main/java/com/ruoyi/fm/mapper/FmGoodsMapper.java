package com.ruoyi.fm.mapper;

import java.util.List;
import com.ruoyi.fm.domain.FmGoods;

/**
 * 商品Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmGoodsMapper 
{
    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    public FmGoods selectFmGoodsById(Long id);

    /**
     * 查询商品列表
     * 
     * @param fmGoods 商品
     * @return 商品集合
     */
    public List<FmGoods> selectFmGoodsList(FmGoods fmGoods);

    /**
     * 新增商品
     * 
     * @param fmGoods 商品
     * @return 结果
     */
    public int insertFmGoods(FmGoods fmGoods);

    /**
     * 修改商品
     * 
     * @param fmGoods 商品
     * @return 结果
     */
    public int updateFmGoods(FmGoods fmGoods);

    /**
     * 删除商品
     * 
     * @param id 商品主键
     * @return 结果
     */
    public int deleteFmGoodsById(Long id);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmGoodsByIds(Long[] ids);
}
