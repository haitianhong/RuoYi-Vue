package com.ruoyi.fm.mapper;

import java.util.List;
import com.ruoyi.fm.domain.FmOrderGoodsDetail;

/**
 * 订单商品详情Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmOrderGoodsDetailMapper 
{
    /**
     * 查询订单商品详情
     * 
     * @param id 订单商品详情主键
     * @return 订单商品详情
     */
    public FmOrderGoodsDetail selectFmOrderGoodsDetailById(Long id);

    /**
     * 查询订单商品详情列表
     * 
     * @param fmOrderGoodsDetail 订单商品详情
     * @return 订单商品详情集合
     */
    public List<FmOrderGoodsDetail> selectFmOrderGoodsDetailList(FmOrderGoodsDetail fmOrderGoodsDetail);

    /**
     * 新增订单商品详情
     * 
     * @param fmOrderGoodsDetail 订单商品详情
     * @return 结果
     */
    public int insertFmOrderGoodsDetail(FmOrderGoodsDetail fmOrderGoodsDetail);

    /**
     * 修改订单商品详情
     * 
     * @param fmOrderGoodsDetail 订单商品详情
     * @return 结果
     */
    public int updateFmOrderGoodsDetail(FmOrderGoodsDetail fmOrderGoodsDetail);

    /**
     * 删除订单商品详情
     * 
     * @param id 订单商品详情主键
     * @return 结果
     */
    public int deleteFmOrderGoodsDetailById(Long id);

    /**
     * 批量删除订单商品详情
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmOrderGoodsDetailByIds(Long[] ids);
}
