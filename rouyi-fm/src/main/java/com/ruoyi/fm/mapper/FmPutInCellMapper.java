package com.ruoyi.fm.mapper;

import java.util.List;

import com.ruoyi.fm.domain.FmPutInCell;

/**
 * 出入土订单Mapper接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface FmPutInCellMapper 
{
    /**
     * 查询出入土订单
     * 
     * @param id 出入土订单主键
     * @return 出入土订单
     */
    public FmPutInCell selectFmPutInCellById(Long id);

    /**
     * 查询出入土订单列表
     * 
     * @param fmPutInCell 出入土订单
     * @return 出入土订单集合
     */
    public List<FmPutInCell> selectFmPutInCellList(FmPutInCell fmPutInCell);

    /**
     * 新增出入土订单
     * 
     * @param fmPutInCell 出入土订单
     * @return 结果
     */
    public int insertFmPutInCell(FmPutInCell fmPutInCell);

    /**
     * 修改出入土订单
     * 
     * @param fmPutInCell 出入土订单
     * @return 结果
     */
    public int updateFmPutInCell(FmPutInCell fmPutInCell);

    /**
     * 删除出入土订单
     * 
     * @param id 出入土订单主键
     * @return 结果
     */
    public int deleteFmPutInCellById(Long id);

    /**
     * 批量删除出入土订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFmPutInCellByIds(Long[] ids);

	public FmPutInCell selectFmPutInCellByOrderId(Long orderId);
}
