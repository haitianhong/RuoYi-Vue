package com.ruoyi.fm.service;

import java.util.List;

import com.ruoyi.fm.domain.FmCellSellOrder;

/**
 * 格位销售Service接口
 *
 * @author ruoyi
 * @date 2021-09-24
 */
public interface IFmCellSellOrderService
{
    /**
     * 查询格位销售
     *
     * @param id 格位销售主键
     * @return 格位销售
     */
    public FmCellSellOrder selectFmCellSellOrderById(Long id);

	/**
	 * 按格位id查询活动订单
	 *
	 * @param cellId 格位主键
	 * @return 格位销售
	 */
	public FmCellSellOrder selectActiveFmCellSellOrder(Long cellId);

    /**
     * 查询格位销售列表
     *
     * @param fmCellSellOrder 格位销售
     * @return 格位销售集合
     */
    public List<FmCellSellOrder> selectFmCellSellOrderList(FmCellSellOrder fmCellSellOrder);

    /**
     * 新增格位销售
     *
     * @param fmCellSellOrder 格位销售
     * @return 结果
     */
    public int insertFmCellSellOrder(FmCellSellOrder fmCellSellOrder);

    /**
     * 修改格位销售
     *
     * @param fmCellSellOrder 格位销售
     * @return 结果
     */
    public int updateFmCellSellOrder(FmCellSellOrder fmCellSellOrder);

    /**
     * 批量删除格位销售
     *
     * @param ids 需要删除的格位销售主键集合
     * @return 结果
     */
    public int deleteFmCellSellOrderByIds(Long[] ids);

    /**
     * 删除格位销售信息
     *
     * @param id 格位销售主键
     * @return 结果
     */
    public int deleteFmCellSellOrderById(Long id);

    public int setFmCellOrderStatus(Long id,Long status);
}
