package com.ruoyi.fm.service;

import java.util.List;

import com.ruoyi.fm.controller.SellParams;
import com.ruoyi.fm.domain.FmCell;
import com.ruoyi.fm.domain.FmCellDetail;

/**
 * 格位Service接口
 *
 * @author ruoyi
 * @date 2021-09-24
 */
public interface IFmCellService
{
    /**
     * 查询格位
     *
     * @param id 格位主键
     * @return 格位
     */
    public FmCell selectFmCellById(String id);

	/**
	 * 查询格位详细信息
	 *
	 * @param id 格位主键
	 * @return 格位详细信息
	 */
	public FmCellDetail selectFmCellDetailById(String id);

	/**
	 * 销售格位
	 *
	 * @param sellParams 格位信息
	 * @return 结果
	 */
	public int sell(SellParams sellParams);

    /**
     * 查询格位列表
     *
     * @param fmCell 格位
     * @return 格位集合
     */
    public List<FmCell> selectFmCellList(FmCell fmCell);

    /**
     * 新增格位
     *
     * @param fmCell 格位
     * @return 结果
     */
    public int insertFmCell(FmCell fmCell);

    /**
     * 修改格位
     *
     * @param fmCell 格位
     * @return 结果
     */
    public int updateFmCell(FmCell fmCell);

    /**
     * 批量删除格位
     *
     * @param ids 需要删除的格位主键集合
     * @return 结果
     */
    public int deleteFmCellByIds(String[] ids);

    /**
     * 删除格位信息
     *
     * @param id 格位主键
     * @return 结果
     */
    public int deleteFmCellById(String id);

	/**
	 * 查询格位详细信息
	 *
	 * @param id 格位主键
	 * @return 格位详细信息
	 */
	public int setFmCellStatus(Long id,Integer status);
}
