package com.ruoyi.fm.service;

import java.util.List;
import com.ruoyi.fm.domain.FmGoodSellOrder;

/**
 * 商品销售Service接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface IFmGoodSellOrderService 
{
    /**
     * 查询商品销售
     * 
     * @param id 商品销售主键
     * @return 商品销售
     */
    public FmGoodSellOrder selectFmGoodSellOrderById(Long id);

    /**
     * 查询商品销售列表
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 商品销售集合
     */
    public List<FmGoodSellOrder> selectFmGoodSellOrderList(FmGoodSellOrder fmGoodSellOrder);

    /**
     * 新增商品销售
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 结果
     */
    public int insertFmGoodSellOrder(FmGoodSellOrder fmGoodSellOrder);

    /**
     * 修改商品销售
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 结果
     */
    public int updateFmGoodSellOrder(FmGoodSellOrder fmGoodSellOrder);

    /**
     * 批量删除商品销售
     * 
     * @param ids 需要删除的商品销售主键集合
     * @return 结果
     */
    public int deleteFmGoodSellOrderByIds(Long[] ids);

    /**
     * 删除商品销售信息
     * 
     * @param id 商品销售主键
     * @return 结果
     */
    public int deleteFmGoodSellOrderById(Long id);
}
