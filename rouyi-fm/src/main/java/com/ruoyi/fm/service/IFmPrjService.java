package com.ruoyi.fm.service;

import java.util.List;
import com.ruoyi.fm.domain.FmPrj;

/**
 * 工程管理Service接口
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
public interface IFmPrjService 
{
    /**
     * 查询工程管理
     * 
     * @param id 工程管理主键
     * @return 工程管理
     */
    public FmPrj selectFmPrjById(Long id);

    /**
     * 查询工程管理列表
     * 
     * @param fmPrj 工程管理
     * @return 工程管理集合
     */
    public List<FmPrj> selectFmPrjList(FmPrj fmPrj);

    /**
     * 新增工程管理
     * 
     * @param fmPrj 工程管理
     * @return 结果
     */
    public int insertFmPrj(FmPrj fmPrj);

    /**
     * 修改工程管理
     * 
     * @param fmPrj 工程管理
     * @return 结果
     */
    public int updateFmPrj(FmPrj fmPrj);

    /**
     * 批量删除工程管理
     * 
     * @param ids 需要删除的工程管理主键集合
     * @return 结果
     */
    public int deleteFmPrjByIds(Long[] ids);

    /**
     * 删除工程管理信息
     * 
     * @param id 工程管理主键
     * @return 结果
     */
    public int deleteFmPrjById(Long id);
}
