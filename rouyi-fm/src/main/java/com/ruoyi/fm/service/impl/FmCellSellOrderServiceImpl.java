package com.ruoyi.fm.service.impl;

import java.util.List;

import com.ruoyi.fm.domain.FmCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.fm.domain.FmCellSellOrder;
import com.ruoyi.fm.mapper.FmCellSellOrderMapper;
import com.ruoyi.fm.service.IFmCellSellOrderService;

/**
 * 格位销售Service业务层处理
 *
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmCellSellOrderServiceImpl implements IFmCellSellOrderService
{
    @Autowired
    private FmCellSellOrderMapper fmCellSellOrderMapper;

    /**
     * 查询格位销售
     *
     * @param id 格位销售主键
     * @return 格位销售
     */
    @Override
    public FmCellSellOrder selectFmCellSellOrderById(Long id)
    {
        return fmCellSellOrderMapper.selectFmCellSellOrderById(id);
    }

    /**
     * 查询格位销售列表
     *
     * @param fmCellSellOrder 格位销售
     * @return 格位销售
     */
    @Override
    public List<FmCellSellOrder> selectFmCellSellOrderList(FmCellSellOrder fmCellSellOrder)
    {
        return fmCellSellOrderMapper.selectFmCellSellOrderList(fmCellSellOrder);
    }

    /**
     * 新增格位销售
     *
     * @param fmCellSellOrder 格位销售
     * @return 结果
     */
    @Override
    public int insertFmCellSellOrder(FmCellSellOrder fmCellSellOrder)
    {
        fmCellSellOrder.setCreateTime(DateUtils.getNowDate());
        return fmCellSellOrderMapper.insertFmCellSellOrder(fmCellSellOrder);
    }

    /**
     * 修改格位销售
     *
     * @param fmCellSellOrder 格位销售
     * @return 结果
     */
    @Override
    public int updateFmCellSellOrder(FmCellSellOrder fmCellSellOrder)
    {
        return fmCellSellOrderMapper.updateFmCellSellOrder(fmCellSellOrder);
    }

    /**
     * 批量删除格位销售
     *
     * @param ids 需要删除的格位销售主键
     * @return 结果
     */
    @Override
    public int deleteFmCellSellOrderByIds(Long[] ids)
    {
        return fmCellSellOrderMapper.deleteFmCellSellOrderByIds(ids);
    }

    /**
     * 删除格位销售信息
     *
     * @param id 格位销售主键
     * @return 结果
     */
    @Override
    public int deleteFmCellSellOrderById(Long id)
    {
        return fmCellSellOrderMapper.deleteFmCellSellOrderById(id);
    }

    @Override
    public int setFmCellOrderStatus(Long id, Long status) {
        FmCellSellOrder fmCellSellOrder = new FmCellSellOrder();
        fmCellSellOrder.setId(id);
        fmCellSellOrder.setStatus(status);
        return updateFmCellSellOrder(fmCellSellOrder);
    }

    @Override
	public FmCellSellOrder selectActiveFmCellSellOrder(Long cellId)
	{
		return fmCellSellOrderMapper.selectActiveFmCellSellOrder(cellId);
	}
}
