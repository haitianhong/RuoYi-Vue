package com.ruoyi.fm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.fm.controller.SellParams;
import com.ruoyi.fm.domain.FmCell;
import com.ruoyi.fm.domain.FmCellDetail;
import com.ruoyi.fm.domain.FmCellSellOrder;
import com.ruoyi.fm.mapper.FmCellMapper;
import com.ruoyi.fm.service.IFmCellSellOrderService;
import com.ruoyi.fm.service.IFmCellService;
import com.ruoyi.fm.service.IFmCustinfoService;
import com.ruoyi.fm.service.IFmPutInCellService;

/**
 * 格位Service业务层处理
 *
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmCellServiceImpl implements IFmCellService
{
    @Autowired
    private FmCellMapper fmCellMapper;

	@Autowired
	private IFmCellSellOrderService cellSellOrderService;

	@Autowired
	private IFmCustinfoService custinfoService;

	@Autowired
	private IFmPutInCellService putInCellService;
    /**
     * 查询格位
     *
     * @param id 格位主键
     * @return 格位
     */
    @Override
    public FmCell selectFmCellById(String id)
    {
        return fmCellMapper.selectFmCellById(id);
    }

    /**
     * 查询格位列表
     *
     * @param fmCell 格位
     * @return 格位
     */
    @Override
    public List<FmCell> selectFmCellList(FmCell fmCell)
    {
        return fmCellMapper.selectFmCellList(fmCell);
    }

    /**
     * 新增格位
     *
     * @param fmCell 格位
     * @return 结果
     */
    @Override
    public int insertFmCell(FmCell fmCell)
    {
        return fmCellMapper.insertFmCell(fmCell);
    }

    /**
     * 修改格位
     *
     * @param fmCell 格位
     * @return 结果
     */
    @Override
    public int updateFmCell(FmCell fmCell)
    {
        return fmCellMapper.updateFmCell(fmCell);
    }

    /**
     * 批量删除格位
     *
     * @param ids 需要删除的格位主键
     * @return 结果
     */
    @Override
    public int deleteFmCellByIds(String[] ids)
    {
        return fmCellMapper.deleteFmCellByIds(ids);
    }

    /**
     * 删除格位信息
     *
     * @param id 格位主键
     * @return 结果
     */
    @Override
    public int deleteFmCellById(String id)
    {
        return fmCellMapper.deleteFmCellById(id);
    }

	@Override
	public int setFmCellStatus(Long id, Integer status) {
		FmCell fmCell = new FmCell();
		fmCell.setId(id);
		fmCell.setStatus(status);
		return updateFmCell(fmCell);
	}

	@Override
	public FmCellDetail selectFmCellDetailById(String id)
	{
		FmCellDetail result = new FmCellDetail();
		result.setCell(this.selectFmCellById(id));

		FmCellSellOrder activeFmCellSellOrder = this.cellSellOrderService.selectActiveFmCellSellOrder(Long.valueOf(id));
		if (activeFmCellSellOrder != null)
		{
			result.setOrder(activeFmCellSellOrder);
			result.setCust(custinfoService.selectFmCustinfoById((Long) (activeFmCellSellOrder.getCustomerId())));
			result.setPutInCell(putInCellService.selectFmPutInCellByOrderId(activeFmCellSellOrder.getId()));
		}

		return result;
	}

	private int doSell(Long id)
	{
		return fmCellMapper.sellFmCell(id);
	}

	@Override
	@Transactional
	public int sell(SellParams sellParams)
	{
		if (this.doSell(sellParams.getCellId()) != 1)
		{
			throw new IllegalStateException("sell failed.");
		}

		LoginUser user = SecurityUtils.getLoginUser();
		FmCellSellOrder order = new FmCellSellOrder();
		order.setCellId(sellParams.getCellId());
		order.setCellName(sellParams.getCellName());
		order.setCreateBy(user.getUsername());
		order.setSalemanId(user.getUser().getUserId());
		order.setSalemanName(sellParams.getSalesName());
		order.setCustomerId(sellParams.getUserId());
		order.setStatus((long) 0);
		order.setPrePay(sellParams.getPrePay());
		order.setRemark(sellParams.getRemark());
		order.setPrize(sellParams.getPrice());

		return cellSellOrderService.insertFmCellSellOrder(order);
	}
}
