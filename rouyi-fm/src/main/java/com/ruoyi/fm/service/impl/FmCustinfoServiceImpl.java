package com.ruoyi.fm.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fm.mapper.FmCustinfoMapper;
import com.ruoyi.fm.domain.FmCustinfo;
import com.ruoyi.fm.service.IFmCustinfoService;

/**
 * 用户信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmCustinfoServiceImpl implements IFmCustinfoService 
{
    @Autowired
    private FmCustinfoMapper fmCustinfoMapper;

    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    @Override
    public FmCustinfo selectFmCustinfoById(Long id)
    {
        return fmCustinfoMapper.selectFmCustinfoById(id);
    }

    /**
     * 查询用户信息列表
     * 
     * @param fmCustinfo 用户信息
     * @return 用户信息
     */
    @Override
    public List<FmCustinfo> selectFmCustinfoList(FmCustinfo fmCustinfo)
    {
        return fmCustinfoMapper.selectFmCustinfoList(fmCustinfo);
    }

    /**
     * 新增用户信息
     * 
     * @param fmCustinfo 用户信息
     * @return 结果
     */
    @Override
    public int insertFmCustinfo(FmCustinfo fmCustinfo)
    {
        return fmCustinfoMapper.insertFmCustinfo(fmCustinfo);
    }

    /**
     * 修改用户信息
     * 
     * @param fmCustinfo 用户信息
     * @return 结果
     */
    @Override
    public int updateFmCustinfo(FmCustinfo fmCustinfo)
    {
        return fmCustinfoMapper.updateFmCustinfo(fmCustinfo);
    }

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteFmCustinfoByIds(Long[] ids)
    {
        return fmCustinfoMapper.deleteFmCustinfoByIds(ids);
    }

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteFmCustinfoById(Long id)
    {
        return fmCustinfoMapper.deleteFmCustinfoById(id);
    }
}
