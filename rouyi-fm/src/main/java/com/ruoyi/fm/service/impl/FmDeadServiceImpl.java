package com.ruoyi.fm.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fm.mapper.FmDeadMapper;
import com.ruoyi.fm.domain.FmDead;
import com.ruoyi.fm.service.IFmDeadService;

/**
 * 逝者信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmDeadServiceImpl implements IFmDeadService 
{
    @Autowired
    private FmDeadMapper fmDeadMapper;

    /**
     * 查询逝者信息
     * 
     * @param id 逝者信息主键
     * @return 逝者信息
     */
    @Override
    public FmDead selectFmDeadById(Long id)
    {
        return fmDeadMapper.selectFmDeadById(id);
    }

    /**
     * 查询逝者信息列表
     * 
     * @param fmDead 逝者信息
     * @return 逝者信息
     */
    @Override
    public List<FmDead> selectFmDeadList(FmDead fmDead)
    {
        return fmDeadMapper.selectFmDeadList(fmDead);
    }

    /**
     * 新增逝者信息
     * 
     * @param fmDead 逝者信息
     * @return 结果
     */
    @Override
    public int insertFmDead(FmDead fmDead)
    {
        return fmDeadMapper.insertFmDead(fmDead);
    }

    /**
     * 修改逝者信息
     * 
     * @param fmDead 逝者信息
     * @return 结果
     */
    @Override
    public int updateFmDead(FmDead fmDead)
    {
        return fmDeadMapper.updateFmDead(fmDead);
    }

    /**
     * 批量删除逝者信息
     * 
     * @param ids 需要删除的逝者信息主键
     * @return 结果
     */
    @Override
    public int deleteFmDeadByIds(Long[] ids)
    {
        return fmDeadMapper.deleteFmDeadByIds(ids);
    }

    /**
     * 删除逝者信息信息
     * 
     * @param id 逝者信息主键
     * @return 结果
     */
    @Override
    public int deleteFmDeadById(Long id)
    {
        return fmDeadMapper.deleteFmDeadById(id);
    }
}
