package com.ruoyi.fm.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.fm.domain.FmGoodSellOrder;
import com.ruoyi.fm.domain.FmOrderGoodsDetail;
import com.ruoyi.fm.mapper.FmGoodSellOrderMapper;
import com.ruoyi.fm.service.IFmGoodSellOrderService;

/**
 * 商品销售Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmGoodSellOrderServiceImpl implements IFmGoodSellOrderService 
{
    @Autowired
    private FmGoodSellOrderMapper fmGoodSellOrderMapper;

    /**
     * 查询商品销售
     * 
     * @param id 商品销售主键
     * @return 商品销售
     */
    @Override
    public FmGoodSellOrder selectFmGoodSellOrderById(Long id)
    {
        return fmGoodSellOrderMapper.selectFmGoodSellOrderById(id);
    }

    /**
     * 查询商品销售列表
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 商品销售
     */
    @Override
    public List<FmGoodSellOrder> selectFmGoodSellOrderList(FmGoodSellOrder fmGoodSellOrder)
    {
        return fmGoodSellOrderMapper.selectFmGoodSellOrderList(fmGoodSellOrder);
    }

    /**
     * 新增商品销售
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 结果
     */
    @Transactional
    @Override
    public int insertFmGoodSellOrder(FmGoodSellOrder fmGoodSellOrder)
    {
        fmGoodSellOrder.setCreateTime(DateUtils.getNowDate());
		fmGoodSellOrder.setLoginId(SecurityUtils.getUserId());
		int result = fmGoodSellOrderMapper.insertFmGoodSellOrder(fmGoodSellOrder);
        insertFmOrderGoodsDetail(fmGoodSellOrder);
		return result;
    }

    /**
     * 修改商品销售
     * 
     * @param fmGoodSellOrder 商品销售
     * @return 结果
     */
    @Transactional
    @Override
    public int updateFmGoodSellOrder(FmGoodSellOrder fmGoodSellOrder)
    {
        fmGoodSellOrderMapper.deleteFmOrderGoodsDetailByOrderId(fmGoodSellOrder.getId());
        insertFmOrderGoodsDetail(fmGoodSellOrder);
        return fmGoodSellOrderMapper.updateFmGoodSellOrder(fmGoodSellOrder);
    }

    /**
     * 批量删除商品销售
     * 
     * @param ids 需要删除的商品销售主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteFmGoodSellOrderByIds(Long[] ids)
    {
        return fmGoodSellOrderMapper.deleteFmGoodSellOrderByIds(ids);
    }

    /**
     * 删除商品销售信息
     * 
     * @param id 商品销售主键
     * @return 结果
     */
    @Override
    public int deleteFmGoodSellOrderById(Long id)
    {
        fmGoodSellOrderMapper.deleteFmOrderGoodsDetailByOrderId(id);
        return fmGoodSellOrderMapper.deleteFmGoodSellOrderById(id);
    }

    /**
     * 新增订单商品详情信息
     * 
     * @param fmGoodSellOrder 商品销售对象
     */
    public void insertFmOrderGoodsDetail(FmGoodSellOrder fmGoodSellOrder)
    {
        List<FmOrderGoodsDetail> fmOrderGoodsDetailList = fmGoodSellOrder.getFmOrderGoodsDetailList();
        Long id = fmGoodSellOrder.getId();
        if (StringUtils.isNotNull(fmOrderGoodsDetailList))
        {
            List<FmOrderGoodsDetail> list = new ArrayList<FmOrderGoodsDetail>();
            for (FmOrderGoodsDetail fmOrderGoodsDetail : fmOrderGoodsDetailList)
            {
                fmOrderGoodsDetail.setOrderId(id);
                list.add(fmOrderGoodsDetail);
            }
            if (list.size() > 0)
            {
                fmGoodSellOrderMapper.batchFmOrderGoodsDetail(list);
            }
        }
    }
}
