package com.ruoyi.fm.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fm.mapper.FmGoodsMapper;
import com.ruoyi.fm.domain.FmGoods;
import com.ruoyi.fm.service.IFmGoodsService;

/**
 * 商品Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmGoodsServiceImpl implements IFmGoodsService 
{
    @Autowired
    private FmGoodsMapper fmGoodsMapper;

    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    @Override
    public FmGoods selectFmGoodsById(Long id)
    {
        return fmGoodsMapper.selectFmGoodsById(id);
    }

    /**
     * 查询商品列表
     * 
     * @param fmGoods 商品
     * @return 商品
     */
    @Override
    public List<FmGoods> selectFmGoodsList(FmGoods fmGoods)
    {
        return fmGoodsMapper.selectFmGoodsList(fmGoods);
    }

    /**
     * 新增商品
     * 
     * @param fmGoods 商品
     * @return 结果
     */
    @Override
    public int insertFmGoods(FmGoods fmGoods)
    {
        return fmGoodsMapper.insertFmGoods(fmGoods);
    }

    /**
     * 修改商品
     * 
     * @param fmGoods 商品
     * @return 结果
     */
    @Override
    public int updateFmGoods(FmGoods fmGoods)
    {
        return fmGoodsMapper.updateFmGoods(fmGoods);
    }

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品主键
     * @return 结果
     */
    @Override
    public int deleteFmGoodsByIds(Long[] ids)
    {
        return fmGoodsMapper.deleteFmGoodsByIds(ids);
    }

    /**
     * 删除商品信息
     * 
     * @param id 商品主键
     * @return 结果
     */
    @Override
    public int deleteFmGoodsById(Long id)
    {
        return fmGoodsMapper.deleteFmGoodsById(id);
    }
}
