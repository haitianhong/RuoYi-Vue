package com.ruoyi.fm.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fm.mapper.FmOrderGoodsDetailMapper;
import com.ruoyi.fm.domain.FmOrderGoodsDetail;
import com.ruoyi.fm.service.IFmOrderGoodsDetailService;

/**
 * 订单商品详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmOrderGoodsDetailServiceImpl implements IFmOrderGoodsDetailService 
{
    @Autowired
    private FmOrderGoodsDetailMapper fmOrderGoodsDetailMapper;

    /**
     * 查询订单商品详情
     * 
     * @param id 订单商品详情主键
     * @return 订单商品详情
     */
    @Override
    public FmOrderGoodsDetail selectFmOrderGoodsDetailById(Long id)
    {
        return fmOrderGoodsDetailMapper.selectFmOrderGoodsDetailById(id);
    }

    /**
     * 查询订单商品详情列表
     * 
     * @param fmOrderGoodsDetail 订单商品详情
     * @return 订单商品详情
     */
    @Override
    public List<FmOrderGoodsDetail> selectFmOrderGoodsDetailList(FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        return fmOrderGoodsDetailMapper.selectFmOrderGoodsDetailList(fmOrderGoodsDetail);
    }

    /**
     * 新增订单商品详情
     * 
     * @param fmOrderGoodsDetail 订单商品详情
     * @return 结果
     */
    @Override
    public int insertFmOrderGoodsDetail(FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        return fmOrderGoodsDetailMapper.insertFmOrderGoodsDetail(fmOrderGoodsDetail);
    }

    /**
     * 修改订单商品详情
     * 
     * @param fmOrderGoodsDetail 订单商品详情
     * @return 结果
     */
    @Override
    public int updateFmOrderGoodsDetail(FmOrderGoodsDetail fmOrderGoodsDetail)
    {
        return fmOrderGoodsDetailMapper.updateFmOrderGoodsDetail(fmOrderGoodsDetail);
    }

    /**
     * 批量删除订单商品详情
     * 
     * @param ids 需要删除的订单商品详情主键
     * @return 结果
     */
    @Override
    public int deleteFmOrderGoodsDetailByIds(Long[] ids)
    {
        return fmOrderGoodsDetailMapper.deleteFmOrderGoodsDetailByIds(ids);
    }

    /**
     * 删除订单商品详情信息
     * 
     * @param id 订单商品详情主键
     * @return 结果
     */
    @Override
    public int deleteFmOrderGoodsDetailById(Long id)
    {
        return fmOrderGoodsDetailMapper.deleteFmOrderGoodsDetailById(id);
    }
}
