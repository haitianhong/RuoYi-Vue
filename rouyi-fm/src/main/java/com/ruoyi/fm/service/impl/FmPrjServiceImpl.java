package com.ruoyi.fm.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.fm.mapper.FmPrjMapper;
import com.ruoyi.fm.domain.FmPrj;
import com.ruoyi.fm.service.IFmPrjService;

/**
 * 工程管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmPrjServiceImpl implements IFmPrjService 
{
    @Autowired
    private FmPrjMapper fmPrjMapper;

    /**
     * 查询工程管理
     * 
     * @param id 工程管理主键
     * @return 工程管理
     */
    @Override
    public FmPrj selectFmPrjById(Long id)
    {
        return fmPrjMapper.selectFmPrjById(id);
    }

    /**
     * 查询工程管理列表
     * 
     * @param fmPrj 工程管理
     * @return 工程管理
     */
    @Override
    public List<FmPrj> selectFmPrjList(FmPrj fmPrj)
    {
        return fmPrjMapper.selectFmPrjList(fmPrj);
    }

    /**
     * 新增工程管理
     * 
     * @param fmPrj 工程管理
     * @return 结果
     */
    @Override
    public int insertFmPrj(FmPrj fmPrj)
    {
        fmPrj.setCreateTime(DateUtils.getNowDate());
        return fmPrjMapper.insertFmPrj(fmPrj);
    }

    /**
     * 修改工程管理
     * 
     * @param fmPrj 工程管理
     * @return 结果
     */
    @Override
    public int updateFmPrj(FmPrj fmPrj)
    {
        return fmPrjMapper.updateFmPrj(fmPrj);
    }

    /**
     * 批量删除工程管理
     * 
     * @param ids 需要删除的工程管理主键
     * @return 结果
     */
    @Override
    public int deleteFmPrjByIds(Long[] ids)
    {
        return fmPrjMapper.deleteFmPrjByIds(ids);
    }

    /**
     * 删除工程管理信息
     * 
     * @param id 工程管理主键
     * @return 结果
     */
    @Override
    public int deleteFmPrjById(Long id)
    {
        return fmPrjMapper.deleteFmPrjById(id);
    }
}
