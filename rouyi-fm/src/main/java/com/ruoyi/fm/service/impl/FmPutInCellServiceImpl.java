package com.ruoyi.fm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.fm.domain.FmPutInCell;
import com.ruoyi.fm.mapper.FmPutInCellMapper;
import com.ruoyi.fm.service.IFmPutInCellService;

/**
 * 出入土订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-09-24
 */
@Service
public class FmPutInCellServiceImpl implements IFmPutInCellService 
{
    @Autowired
    private FmPutInCellMapper fmPutInCellMapper;

    /**
     * 查询出入土订单
     * 
     * @param id 出入土订单主键
     * @return 出入土订单
     */
    @Override
    public FmPutInCell selectFmPutInCellById(Long id)
    {
        return fmPutInCellMapper.selectFmPutInCellById(id);
    }

    /**
     * 查询出入土订单列表
     * 
     * @param fmPutInCell 出入土订单
     * @return 出入土订单
     */
    @Override
    public List<FmPutInCell> selectFmPutInCellList(FmPutInCell fmPutInCell)
    {
        return fmPutInCellMapper.selectFmPutInCellList(fmPutInCell);
    }

    /**
     * 新增出入土订单
     * 
     * @param fmPutInCell 出入土订单
     * @return 结果
     */
    @Override
    public int insertFmPutInCell(FmPutInCell fmPutInCell)
    {
        return fmPutInCellMapper.insertFmPutInCell(fmPutInCell);
    }

    /**
     * 修改出入土订单
     * 
     * @param fmPutInCell 出入土订单
     * @return 结果
     */
    @Override
    public int updateFmPutInCell(FmPutInCell fmPutInCell)
    {
        return fmPutInCellMapper.updateFmPutInCell(fmPutInCell);
    }

    /**
     * 批量删除出入土订单
     * 
     * @param ids 需要删除的出入土订单主键
     * @return 结果
     */
    @Override
    public int deleteFmPutInCellByIds(Long[] ids)
    {
        return fmPutInCellMapper.deleteFmPutInCellByIds(ids);
    }

    /**
     * 删除出入土订单信息
     * 
     * @param id 出入土订单主键
     * @return 结果
     */
    @Override
    public int deleteFmPutInCellById(Long id)
    {
        return fmPutInCellMapper.deleteFmPutInCellById(id);
    }

	@Override
	public FmPutInCell selectFmPutInCellByOrderId(Long orderId)
	{
		return fmPutInCellMapper.selectFmPutInCellByOrderId(orderId);
	}
}
