import request from '@/utils/request'

// 查询格位列表
export function listCell(query) {
  return request({
    url: '/fm/cell/list',
    method: 'get',
    params: query
  })
}

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/fm/custinfo/listForSell',
    method: 'get',
    params: query
  })
}

// 查询格位详细
export function getCell(id) {
  return request({
    url: '/fm/cell/' + id,
    method: 'get'
  })
}

// 查询格位详细信息，包括订单等
export function getCellDetail(id) {
  return request({
    url: '/fm/cell/detail/' + id,
    method: 'get'
  })
}

// 新增格位
export function addCell(data) {
  return request({
    url: '/fm/cell',
    method: 'post',
    data: data
  })
}

// 修改格位
export function updateCell(data) {
  return request({
    url: '/fm/cell',
    method: 'put',
    data: data
  })
}

// 删除格位
export function delCell(id) {
  return request({
    url: '/fm/cell/' + id,
    method: 'delete'
  })
}

// 删除格位
export function sellCell(query) {
  return request({
    url: '/fm/cell/sell',
    method: 'put',
    data: query
  })
}

// 导出格位
export function exportCell(query) {
  return request({
    url: '/fm/cell/export',
    method: 'get',
    params: query
  })
}