import request from '@/utils/request'

// 查询格位销售列表
export function listCellOrder(query) {
  return request({
    url: '/fm/cellOrder/list',
    method: 'get',
    params: query
  })
}

// 查询格位销售详细
export function getCellOrder(id) {
  return request({
    url: '/fm/cellOrder/' + id,
    method: 'get'
  })
}

// 查询格位详细信息，包括订单等
export function getCellDetail(query) {
  return request({
    url: '/fm/cellOrder/detail/',
    method: 'get',
    params: query
  })
}

// 新增格位销售
export function addCellOrder(data) {
  return request({
    url: '/fm/cellOrder',
    method: 'post',
    data: data
  })
}

// 修改格位销售
export function updateCellOrder(data) {
  return request({
    url: '/fm/cellOrder',
    method: 'put',
    data: data
  })
}

// 删除格位销售
export function delCellOrder(id) {
  return request({
    url: '/fm/cellOrder/' + id,
    method: 'delete'
  })
}

// 导出格位销售
export function exportCellOrder(query) {
  return request({
    url: '/fm/cellOrder/export',
    method: 'get',
    params: query
  })
}

  // 导出格位销售
  export function endCellOrder(id,cellId) {
    return request({
      url: '/fm/cellOrder/endCellOrder/ '+ id + '/' + cellId,
      method: 'post'
    })
}
