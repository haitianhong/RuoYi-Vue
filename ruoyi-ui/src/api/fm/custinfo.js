import request from '@/utils/request'

// 查询用户信息列表
export function listCustinfo(query) {
  return request({
    url: '/fm/custinfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户信息详细
export function getCustinfo(id) {
  return request({
    url: '/fm/custinfo/' + id,
    method: 'get'
  })
}

// 新增用户信息
export function addCustinfo(data) {
  return request({
    url: '/fm/custinfo',
    method: 'post',
    data: data
  })
}

// 修改用户信息
export function updateCustinfo(data) {
  return request({
    url: '/fm/custinfo',
    method: 'put',
    data: data
  })
}

// 删除用户信息
export function delCustinfo(id) {
  return request({
    url: '/fm/custinfo/' + id,
    method: 'delete'
  })
}

// 导出用户信息
export function exportCustinfo(query) {
  return request({
    url: '/fm/custinfo/export',
    method: 'get',
    params: query
  })
}