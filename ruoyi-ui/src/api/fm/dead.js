import request from '@/utils/request'

// 查询逝者信息列表
export function listDead(query) {
  return request({
    url: '/fm/dead/list',
    method: 'get',
    params: query
  })
}

// 查询逝者信息详细
export function getDead(id) {
  return request({
    url: '/fm/dead/' + id,
    method: 'get'
  })
}

// 新增逝者信息
export function addDead(data) {
  return request({
    url: '/fm/dead',
    method: 'post',
    data: data
  })
}

// 修改逝者信息
export function updateDead(data) {
  return request({
    url: '/fm/dead',
    method: 'put',
    data: data
  })
}

// 删除逝者信息
export function delDead(id) {
  return request({
    url: '/fm/dead/' + id,
    method: 'delete'
  })
}

// 导出逝者信息
export function exportDead(query) {
  return request({
    url: '/fm/dead/export',
    method: 'get',
    params: query
  })
}