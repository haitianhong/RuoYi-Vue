import request from '@/utils/request'

// 查询订单商品详情列表
export function listDetail(query) {
  return request({
    url: '/fm/detail/list',
    method: 'get',
    params: query
  })
}

// 查询订单商品详情详细
export function getDetail(id) {
  return request({
    url: '/fm/detail/' + id,
    method: 'get'
  })
}

// 新增订单商品详情
export function addDetail(data) {
  return request({
    url: '/fm/detail',
    method: 'post',
    data: data
  })
}

// 修改订单商品详情
export function updateDetail(data) {
  return request({
    url: '/fm/detail',
    method: 'put',
    data: data
  })
}

// 删除订单商品详情
export function delDetail(id) {
  return request({
    url: '/fm/detail/' + id,
    method: 'delete'
  })
}

// 导出订单商品详情
export function exportDetail(query) {
  return request({
    url: '/fm/detail/export',
    method: 'get',
    params: query
  })
}