import request from '@/utils/request'

// 查询商品销售列表
export function listGoodOrder(query) {
  return request({
    url: '/fm/goodOrder/list',
    method: 'get',
    params: query
  })
}

// 查询商品销售详细
export function getGoodOrder(id) {
  return request({
    url: '/fm/goodOrder/' + id,
    method: 'get'
  })
}

// 新增商品销售
export function addGoodOrder(data) {
  return request({
    url: '/fm/goodOrder',
    method: 'post',
    data: data
  })
}

// 修改商品销售
export function updateGoodOrder(data) {
  return request({
    url: '/fm/goodOrder',
    method: 'put',
    data: data
  })
}

// 删除商品销售
export function delGoodOrder(id) {
  return request({
    url: '/fm/goodOrder/' + id,
    method: 'delete'
  })
}

// 导出商品销售
export function exportGoodOrder(query) {
  return request({
    url: '/fm/goodOrder/export',
    method: 'get',
    params: query
  })
}