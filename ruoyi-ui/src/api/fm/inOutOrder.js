import request from '@/utils/request'

// 查询出入土订单列表
export function listInOutOrder(query) {
  query.queryParams = "id";
  query.isAsc = "desc"
  return request({
    url: '/fm/inOutOrder/list',
    method: 'get',
    params: query
  })
}

// 查询出入土订单详细
export function getInOutOrder(id) {
  return request({
    url: '/fm/inOutOrder/' + id,
    method: 'get'
  })
}

// 新增出入土订单
export function addInOutOrder(data) {
  return request({
    url: '/fm/inOutOrder',
    method: 'post',
    data: data
  })
}

// 新增出入土订单
export function addOutOrder(data) {
  return request({
    url: '/fm/inOutOrder/OutOrder',
    method: 'post',
    data: data
  })
}

// 修改出入土订单
export function updateInOutOrder(data) {
  return request({
    url: '/fm/inOutOrder',
    method: 'put',
    data: data
  })
}

// 删除出入土订单
export function delInOutOrder(id) {
  return request({
    url: '/fm/inOutOrder/' + id,
    method: 'delete'
  })
}

// 导出出入土订单
export function exportInOutOrder(query) {
  return request({
    url: '/fm/inOutOrder/export',
    method: 'get',
    params: query
  })
}
