import request from '@/utils/request'

// 查询工程管理列表
export function listPrj(query) {
  return request({
    url: '/fm/prj/list',
    method: 'get',
    params: query
  })
}

// 查询工程管理详细
export function getPrj(id) {
  return request({
    url: '/fm/prj/' + id,
    method: 'get'
  })
}

// 新增工程管理
export function addPrj(data) {
  return request({
    url: '/fm/prj',
    method: 'post',
    data: data
  })
}

// 修改工程管理
export function updatePrj(data) {
  return request({
    url: '/fm/prj',
    method: 'put',
    data: data
  })
}

// 删除工程管理
export function delPrj(id) {
  return request({
    url: '/fm/prj/' + id,
    method: 'delete'
  })
}

// 导出工程管理
export function exportPrj(query) {
  return request({
    url: '/fm/prj/export',
    method: 'get',
    params: query
  })
}