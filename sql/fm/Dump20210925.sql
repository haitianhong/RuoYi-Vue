CREATE DATABASE  IF NOT EXISTS `ruoyi-vue` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `ruoyi-vue`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ruoyi-vue
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fm_cell`
--

DROP TABLE IF EXISTS `fm_cell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_cell` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `location` varchar(32) DEFAULT NULL COMMENT '位置',
  `price` decimal(16,2) DEFAULT NULL COMMENT '价格',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `good_type` int(11) DEFAULT NULL COMMENT '产品编码',
  `status` int(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='格位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_cell`
--

LOCK TABLES `fm_cell` WRITE;
/*!40000 ALTER TABLE `fm_cell` DISABLE KEYS */;
INSERT INTO `fm_cell` VALUES (1,'dsf','fghfgh',23.00,1,NULL,0);
/*!40000 ALTER TABLE `fm_cell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_cell_sell_order`
--

DROP TABLE IF EXISTS `fm_cell_sell_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_cell_sell_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `login_id` int(11) DEFAULT NULL COMMENT '创建人',
  `saleman_id` int(11) NOT NULL COMMENT '销售人员ID',
  `saleman_name` varchar(45) NOT NULL COMMENT '销售人员',
  `customer_id` int(10) NOT NULL COMMENT '用户ID',
  `customer_name` varchar(45) DEFAULT NULL COMMENT '用户',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `prize` int(11) DEFAULT NULL COMMENT '金额',
  `cell_id` int(11) NOT NULL COMMENT ' 格位ID',
  `cell_name` varchar(45) DEFAULT NULL COMMENT '格位名称',
  `pre_pay` int(11) DEFAULT NULL COMMENT '预付金额',
  `remark` varchar(32) DEFAULT NULL COMMENT '备注',
  `start_time` datetime DEFAULT NULL COMMENT '起始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='格位销售';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_cell_sell_order`
--

LOCK TABLES `fm_cell_sell_order` WRITE;
/*!40000 ALTER TABLE `fm_cell_sell_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `fm_cell_sell_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_custinfo`
--

DROP TABLE IF EXISTS `fm_custinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_custinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cust_name` varchar(24) NOT NULL COMMENT '客户姓名',
  `sex` varchar(1) NOT NULL COMMENT '性别',
  `number` varchar(36) NOT NULL COMMENT '身份证',
  `photone` varchar(24) NOT NULL COMMENT '手机号',
  `addr` varchar(64) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_custinfo`
--

LOCK TABLES `fm_custinfo` WRITE;
/*!40000 ALTER TABLE `fm_custinfo` DISABLE KEYS */;
INSERT INTO `fm_custinfo` VALUES (1,'aaa','0','121321','123123',NULL);
/*!40000 ALTER TABLE `fm_custinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_dead`
--

DROP TABLE IF EXISTS `fm_dead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_dead` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `deceased` varchar(50) NOT NULL COMMENT '逝者姓名',
  `sex` int(11) NOT NULL COMMENT '性别',
  `number` varchar(50) NOT NULL COMMENT '身份证号码',
  `deceased_date` datetime NOT NULL COMMENT '逝世日期',
  `memo` varchar(64) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='逝者信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_dead`
--

LOCK TABLES `fm_dead` WRITE;
/*!40000 ALTER TABLE `fm_dead` DISABLE KEYS */;
INSERT INTO `fm_dead` VALUES (1,'bb',0,'123123123','2021-09-19 00:00:00',NULL);
/*!40000 ALTER TABLE `fm_dead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_good_sell_order`
--

DROP TABLE IF EXISTS `fm_good_sell_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_good_sell_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `login_id` int(11) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `serial` varchar(32) NOT NULL COMMENT '订单编号',
  `money` varchar(32) DEFAULT NULL COMMENT '金额',
  `remark` varchar(32) DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品销售';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_good_sell_order`
--

LOCK TABLES `fm_good_sell_order` WRITE;
/*!40000 ALTER TABLE `fm_good_sell_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `fm_good_sell_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_good_type`
--

DROP TABLE IF EXISTS `fm_good_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_good_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) DEFAULT NULL COMMENT '类型名称',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_good_type`
--

LOCK TABLES `fm_good_type` WRITE;
/*!40000 ALTER TABLE `fm_good_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `fm_good_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_goods`
--

DROP TABLE IF EXISTS `fm_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) NOT NULL COMMENT '商品名称',
  `good_type` int(11) NOT NULL COMMENT '商品类型ID',
  `price` decimal(24,2) NOT NULL COMMENT '商品单价',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_goods`
--

LOCK TABLES `fm_goods` WRITE;
/*!40000 ALTER TABLE `fm_goods` DISABLE KEYS */;
INSERT INTO `fm_goods` VALUES (1,'花',1,10.00,NULL,NULL);
/*!40000 ALTER TABLE `fm_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_order_goods_detail`
--

DROP TABLE IF EXISTS `fm_order_goods_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_order_goods_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(10) NOT NULL COMMENT ' 商品名称',
  `prize` double NOT NULL COMMENT '单价',
  `num` int(11) NOT NULL COMMENT '数量',
  `notice` varchar(32) DEFAULT NULL COMMENT '备注',
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单商品详情';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_order_goods_detail`
--

LOCK TABLES `fm_order_goods_detail` WRITE;
/*!40000 ALTER TABLE `fm_order_goods_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `fm_order_goods_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_prj`
--

DROP TABLE IF EXISTS `fm_prj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_prj` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `login_id` int(11) NOT NULL COMMENT '登录ID',
  `create_by` varchar(50) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `remark` text NOT NULL COMMENT '问题描述',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `operator` varchar(32) DEFAULT NULL COMMENT '办理人',
  `status` int(11) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='工程管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_prj`
--

LOCK TABLES `fm_prj` WRITE;
/*!40000 ALTER TABLE `fm_prj` DISABLE KEYS */;
/*!40000 ALTER TABLE `fm_prj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fm_put_in_cell`
--

DROP TABLE IF EXISTS `fm_put_in_cell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fm_put_in_cell` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `login_id` int(11) NOT NULL COMMENT '登录id',
  `operator_name` varchar(32) NOT NULL COMMENT '办理人姓名',
  `operator_identification` varchar(32) NOT NULL COMMENT '办理人身份证',
  `operator_phone` varchar(32) DEFAULT NULL COMMENT '办理人手机号码',
  `cell_id` int(11) NOT NULL COMMENT '墓园ID',
  `cell_name` varchar(45) DEFAULT NULL,
  `death_id` int(11) NOT NULL COMMENT '逝者ID',
  `death_name` varchar(45) DEFAULT NULL COMMENT '逝者',
  `time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  `op_type` int(11) DEFAULT NULL COMMENT '操作类型',
  `cell_order_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='出入土订单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fm_put_in_cell`
--

LOCK TABLES `fm_put_in_cell` WRITE;
/*!40000 ALTER TABLE `fm_put_in_cell` DISABLE KEYS */;
/*!40000 ALTER TABLE `fm_put_in_cell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_table`
--

DROP TABLE IF EXISTS `gen_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_table`
--

LOCK TABLES `gen_table` WRITE;
/*!40000 ALTER TABLE `gen_table` DISABLE KEYS */;
INSERT INTO `gen_table` VALUES (21,'fm_cell','格位','','','FmCell','crud','com.ruoyi.fm','fm','cell','格位','ruoyi','0','/','{\"parentMenuId\":\"4\"}','admin','2021-09-12 16:39:33','','2021-09-24 15:40:22',NULL),(22,'fm_cell_sell_order','格位销售','','','FmCellSellOrder','crud','com.ruoyi.fm','fm','cellOrder','格位销售','ruoyi','0','/','{\"treeCode\":\"customer_id\",\"parentMenuId\":\"4\"}','admin','2021-09-12 16:39:34','','2021-09-24 16:07:04',NULL),(23,'fm_custinfo','用户信息',NULL,NULL,'FmCustinfo','crud','com.ruoyi.fm','fm','custinfo','用户信息','ruoyi','0','/','{\"parentMenuId\":4}','admin','2021-09-12 16:39:34','','2021-09-16 00:02:41',NULL),(24,'fm_dead','逝者信息',NULL,NULL,'FmDead','crud','com.ruoyi.fm','fm','dead','逝者信息','ruoyi','0','/','{\"parentMenuId\":4}','admin','2021-09-12 16:39:34','','2021-09-16 00:03:02',NULL),(25,'fm_good_sell_order','商品销售','fm_order_goods_detail','order_id','FmGoodSellOrder','sub','com.ruoyi.fm','fm','goodOrder','商品销售','ruoyi','0','/','{\"parentMenuId\":\"4\"}','admin','2021-09-12 16:39:34','','2021-09-24 17:08:36',NULL),(27,'fm_goods','商品',NULL,NULL,'FmGoods','crud','com.ruoyi.fm','fm','goods','商品','ruoyi','0','/','{\"parentMenuId\":4}','admin','2021-09-12 16:39:34','','2021-09-16 00:03:13',NULL),(28,'fm_order_goods_detail','订单商品详情',NULL,NULL,'FmOrderGoodsDetail','crud','com.ruoyi.fm','fm','detail','订单商品详情','ruoyi','0','/','{\"parentMenuId\":4}','admin','2021-09-12 16:39:34','','2021-09-16 00:03:25',NULL),(29,'fm_prj','工程管理',NULL,NULL,'FmPrj','crud','com.ruoyi.fm','fm','prj','工程管理','ruoyi','0','/','{\"parentMenuId\":\"4\"}','admin','2021-09-12 16:39:34','','2021-09-24 15:23:16',NULL),(30,'fm_put_in_cell','出入土订单',NULL,NULL,'FmPutInCell','crud','com.ruoyi.fm','fm','inOutOrder','出入土订单','ruoyi','0','/','{\"parentMenuId\":\"4\"}','admin','2021-09-12 16:39:34','','2021-09-24 17:16:43',NULL);
/*!40000 ALTER TABLE `gen_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_table_column`
--

DROP TABLE IF EXISTS `gen_table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='代码生成业务表字段';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_table_column`
--

LOCK TABLES `gen_table_column` WRITE;
/*!40000 ALTER TABLE `gen_table_column` DISABLE KEYS */;
INSERT INTO `gen_table_column` VALUES (169,'21','id','主键','varchar(36)','String','id','1','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:33','','2021-09-24 15:40:22'),(170,'21','name','名称','varchar(32)','String','name','0','0',NULL,'1','1','1','1','LIKE','input','',2,'admin','2021-09-12 16:39:33','','2021-09-24 15:40:22'),(171,'21','location','位置','varchar(32)','String','location','0','0',NULL,'1','1','1','1','LIKE','input','',3,'admin','2021-09-12 16:39:33','','2021-09-24 15:40:22'),(172,'21','price','价格','decimal(16,2)','Double','price','0','0',NULL,'1','1','1',NULL,'EQ','input','',4,'admin','2021-09-12 16:39:34','','2021-09-24 15:40:22'),(173,'21','type','类型','int(11)','Integer','type','0','0',NULL,'1',NULL,NULL,'1','EQ','select','fm_cell_type',5,'admin','2021-09-12 16:39:34','','2021-09-24 15:40:22'),(174,'21','status','状态','int(10)','Integer','status','0','0',NULL,'1','1','1','1','EQ','radio','fm_sell_status',6,'admin','2021-09-12 16:39:34','','2021-09-24 15:40:22'),(175,'22','id','主键','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(176,'22','login_id','创建人','int(11)','Long','loginId','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','input','',2,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(177,'22','customer_id','用户ID','int(10)','Integer','customerId','0','0','1','1',NULL,NULL,NULL,'EQ','input','',3,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(178,'22','customer_name','用户','varchar(45)','String','customerName','0','0',NULL,NULL,NULL,'1','1','LIKE','input','',4,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(179,'22','create_time','创建日期','datetime','Date','createTime','0','0',NULL,NULL,NULL,'1',NULL,'EQ','datetime','',5,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(180,'22','prize','金额','int(11)','Long','prize','0','0',NULL,'1','1','1',NULL,'EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(181,'22','cell_name','格位名称','varchar(45)','String','cellName','0','0',NULL,NULL,NULL,'1','1','LIKE','input','',7,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(182,'22','pre_pay','预付金额','int(11)','Long','prePay','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','input','',8,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(183,'22','cell_id','订单编号','int(11)','Long','cellId','0','0',NULL,'1',NULL,NULL,'1','EQ','input','',9,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(184,'22','remark','备注','varchar(32)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',10,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(185,'22','start_time','起始时间','datetime','Date','startTime','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','datetime','',11,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(186,'22','end_time','结束时间','datetime','Date','endTime','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','datetime','',12,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(187,'22','status','状态','int(11)','Long','status','0','0',NULL,NULL,'1','1','1','EQ','radio','',13,'admin','2021-09-12 16:39:34','','2021-09-24 16:07:04'),(188,'23','id','主键','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-16 00:02:41'),(189,'23','cust_name','客户姓名','varchar(24)','String','custName','0','0','1','1','1','1','1','LIKE','input','',2,'admin','2021-09-12 16:39:34','','2021-09-16 00:02:41'),(190,'23','sex','性别','varchar(1)','String','sex','0','0','1','1','1','1',NULL,'EQ','select','sys_user_sex',3,'admin','2021-09-12 16:39:34','','2021-09-16 00:02:41'),(191,'23','number','身份证','varchar(36)','String','number','0','0','1','1','1','1','1','EQ','input','',4,'admin','2021-09-12 16:39:34','','2021-09-16 00:02:41'),(192,'23','photone','手机号','varchar(24)','String','photone','0','0','1','1','1','1','1','EQ','input','',5,'admin','2021-09-12 16:39:34','','2021-09-16 00:02:41'),(193,'23','addr','地址','varchar(64)','String','addr','0','0',NULL,'1','1','1',NULL,'EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-16 00:02:41'),(194,'24','id','主键','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:02'),(195,'24','deceased','逝者姓名','varchar(50)','String','deceased','0','0','1','1','1','1','1','LIKE','input','',2,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:02'),(196,'24','sex','性别','int(11)','Long','sex','0','0','1','1','1','1','1','EQ','select','sys_user_sex',3,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:02'),(197,'24','number','身份证号码','varchar(50)','String','number','0','0','1','1','1','1','1','EQ','input','',4,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:02'),(198,'24','deceased_date','逝世日期','datetime','Date','deceasedDate','0','0','1','1','1','1',NULL,'EQ','datetime','',5,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:02'),(199,'24','memo','备注','varchar(64)','String','memo','0','0',NULL,'1','1','1',NULL,'EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:02'),(200,'25','id','主键','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-24 17:08:36'),(201,'25','login_id','创建人','int(11)','Long','loginId','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','input','',2,'admin','2021-09-12 16:39:34','','2021-09-24 17:08:36'),(202,'25','create_time','创建日期','datetime','Date','createTime','0','0',NULL,NULL,NULL,'1','1','BETWEEN','datetime','',3,'admin','2021-09-12 16:39:34','','2021-09-24 17:08:36'),(204,'25','money','金额','varchar(32)','String','money','0','0',NULL,'1',NULL,'1',NULL,'EQ','input','',5,'admin','2021-09-12 16:39:34','','2021-09-24 17:08:36'),(205,'25','remark','备注','varchar(32)','String','remark','0','0',NULL,'1',NULL,'1',NULL,'EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-24 17:08:36'),(206,'25','status','状态','int(11)','Long','status','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','radio','',7,'admin','2021-09-12 16:39:34','','2021-09-24 17:08:36'),(210,'27','id','主键','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:13'),(211,'27','name','商品名称','varchar(32)','String','name','0','0','1','1','1','1','1','LIKE','input','',2,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:13'),(212,'27','good_type','商品类型ID','int(11)','Long','goodType','0','0','1','1','1','1','1','EQ','select','fm_good_type',3,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:13'),(214,'27','price','商品单价','decimal(24,2)','BigDecimal','price','0','0',NULL,'1','1','1',NULL,'EQ','input','',5,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:13'),(215,'27','num','数量','int(11)','Long','num','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:13'),(216,'27','remark','备注','varchar(128)','String','remark','0','0',NULL,'1','1','1',NULL,'EQ','input','',7,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:13'),(217,'28','id','ID','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:25'),(218,'28','name',' 商品名称','varchar(10)','String','name','0','0','1','1',NULL,'1',NULL,'EQ','input','',2,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:25'),(219,'28','prize','单价','double','Long','prize','0','0','1','1',NULL,'1',NULL,'EQ','input','',3,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:25'),(220,'28','num','数量','int(11)','Long','num','0','0','1','1',NULL,'1',NULL,'EQ','input','',4,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:25'),(221,'28','notice','备注','varchar(32)','String','notice','0','0',NULL,'1',NULL,'1',NULL,'EQ','input','',5,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:25'),(222,'28','order_id','订单ID','int(11)','Long','orderId','0','0','1','1',NULL,'1','1','EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-16 00:03:25'),(223,'29','id','主键','int(11)','Long','id','1','1',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(224,'29','login_id','登录ID','int(11)','Long','loginId','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','input','',2,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(225,'29','create_by','创建人','varchar(50)','String','createBy','0','0','1','1','1','1','1','LIKE','input','',3,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(226,'29','create_time','创建日期','datetime','Date','createTime','0','0','1',NULL,NULL,'1','1','BETWEEN','datetime','',4,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(227,'29','remark','问题描述','text','String','remark','0','0','1','1','1','1','1','LIKE','textarea','',5,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(228,'29','end_time','结束时间','datetime','Date','endTime','0','0',NULL,NULL,'1','1',NULL,'EQ','datetime','',6,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(229,'29','operator','办理人','varchar(32)','String','operator','0','0',NULL,'1','1','1','1','LIKE','input','',7,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(230,'29','status','状态','int(11)','Long','status','0','0','1',NULL,'1','1','1','EQ','radio','sys_notice_status',8,'admin','2021-09-12 16:39:34','','2021-09-24 15:23:16'),(231,'30','id','ID','int(11)','Long','id','1','0',NULL,'1',NULL,NULL,NULL,'EQ','input','',1,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(232,'30','login_id','登录id','int(11)','Long','loginId','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','input','',2,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(233,'30','operator_name','办理人姓名','varchar(32)','String','operatorName','0','0','1','1','1','1','1','LIKE','input','',3,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(234,'30','operator_identification','办理人身份证','varchar(32)','String','operatorIdentification','0','0','1','1','1','1','1','EQ','input','',4,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(235,'30','operator_phone','办理人手机号码','varchar(32)','String','operatorPhone','0','0','1','1','1','1','1','EQ','input','',5,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(236,'30','cell_id','墓园ID','int(11)','Long','cellId','0','0','1','1',NULL,NULL,NULL,'EQ','input','',6,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(237,'30','cell_name',NULL,'varchar(45)','String','cellName','0','0',NULL,NULL,NULL,'1','1','LIKE','input','',7,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(238,'30','death_id','逝者ID','int(11)','Long','deathId','0','0','1','1',NULL,NULL,NULL,'EQ','input','',8,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(239,'30','death_name','逝者','varchar(45)','String','deathName','0','0',NULL,'1',NULL,'1','1','LIKE','input','',9,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(240,'30','time','时间','datetime','Date','time','0','0',NULL,NULL,NULL,'1','1','BETWEEN','datetime','',10,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(241,'30','op_type','操作类型','int(11)','Long','opType','0','0',NULL,'1',NULL,'1','1','EQ','select','fm_cell_op',11,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(242,'30','cell_order_id',NULL,'int(11)','Long','cellOrderId','0','0',NULL,'1',NULL,'1',NULL,'EQ','input','',12,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(243,'30','status','状态','int(11)','Long','status','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','radio','',13,'admin','2021-09-12 16:39:34','','2021-09-24 17:16:43'),(244,'21','good_type','产品编码','int(11)','Long','goodType','0','0',NULL,NULL,NULL,NULL,NULL,'EQ','select','',6,'','2021-09-15 23:21:13','','2021-09-24 15:40:22'),(245,'22','saleman_id','销售人员ID','int(11)','Long','salemanId','0','0','1','1',NULL,NULL,NULL,'EQ','input','',3,'','2021-09-24 15:53:10','','2021-09-24 16:07:04'),(246,'22','saleman_name','销售人员','varchar(45)','String','salemanName','0','0','1',NULL,NULL,'1','1','LIKE','input','',4,'','2021-09-24 15:53:10','','2021-09-24 16:07:04'),(247,'25','serial','订单编号','varchar(32)','String','serial','0','0','1',NULL,NULL,'1','1','EQ','input','',4,'','2021-09-24 16:16:08','','2021-09-24 17:08:36');
/*!40000 ALTER TABLE `gen_table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_blob_triggers`
--

DROP TABLE IF EXISTS `qrtz_blob_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Blob类型的触发器表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_blob_triggers`
--

LOCK TABLES `qrtz_blob_triggers` WRITE;
/*!40000 ALTER TABLE `qrtz_blob_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_blob_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_calendars`
--

DROP TABLE IF EXISTS `qrtz_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='日历信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_calendars`
--

LOCK TABLES `qrtz_calendars` WRITE;
/*!40000 ALTER TABLE `qrtz_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_cron_triggers`
--

DROP TABLE IF EXISTS `qrtz_cron_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Cron类型的触发器表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_cron_triggers`
--

LOCK TABLES `qrtz_cron_triggers` WRITE;
/*!40000 ALTER TABLE `qrtz_cron_triggers` DISABLE KEYS */;
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler','TASK_CLASS_NAME1','DEFAULT','0/10 * * * * ?','Asia/Shanghai'),('RuoyiScheduler','TASK_CLASS_NAME2','DEFAULT','0/15 * * * * ?','Asia/Shanghai'),('RuoyiScheduler','TASK_CLASS_NAME3','DEFAULT','0/20 * * * * ?','Asia/Shanghai');
/*!40000 ALTER TABLE `qrtz_cron_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_fired_triggers`
--

DROP TABLE IF EXISTS `qrtz_fired_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) NOT NULL COMMENT '状态',
  `job_name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='已触发的触发器表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_fired_triggers`
--

LOCK TABLES `qrtz_fired_triggers` WRITE;
/*!40000 ALTER TABLE `qrtz_fired_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_fired_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_job_details`
--

DROP TABLE IF EXISTS `qrtz_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='任务详细信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_job_details`
--

LOCK TABLES `qrtz_job_details` WRITE;
/*!40000 ALTER TABLE `qrtz_job_details` DISABLE KEYS */;
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler','TASK_CLASS_NAME1','DEFAULT',NULL,'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution','0','1','0','0','�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0TASK_PROPERTIESsr\0com.ruoyi.quartz.domain.SysJob\0\0\0\0\0\0\0\0L\0\nconcurrentt\0Ljava/lang/String;L\0cronExpressionq\0~\0	L\0invokeTargetq\0~\0	L\0jobGroupq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0jobNameq\0~\0	L\0\rmisfirePolicyq\0~\0	L\0statusq\0~\0	xr\0\'com.ruoyi.common.core.domain.BaseEntity\0\0\0\0\0\0\0\0L\0createByq\0~\0	L\0\ncreateTimet\0Ljava/util/Date;L\0paramsq\0~\0L\0remarkq\0~\0	L\0searchValueq\0~\0	L\0updateByq\0~\0	L\0\nupdateTimeq\0~\0xpt\0adminsr\0java.util.Datehj�KYt\0\0xpw\0\0{՗Ƹxpt\0\0pppt\01t\00/10 * * * * ?t\0ryTask.ryNoParamst\0DEFAULTsr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0系统默认（无参）t\03t\01x\0'),('RuoyiScheduler','TASK_CLASS_NAME2','DEFAULT',NULL,'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution','0','1','0','0','�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0TASK_PROPERTIESsr\0com.ruoyi.quartz.domain.SysJob\0\0\0\0\0\0\0\0L\0\nconcurrentt\0Ljava/lang/String;L\0cronExpressionq\0~\0	L\0invokeTargetq\0~\0	L\0jobGroupq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0jobNameq\0~\0	L\0\rmisfirePolicyq\0~\0	L\0statusq\0~\0	xr\0\'com.ruoyi.common.core.domain.BaseEntity\0\0\0\0\0\0\0\0L\0createByq\0~\0	L\0\ncreateTimet\0Ljava/util/Date;L\0paramsq\0~\0L\0remarkq\0~\0	L\0searchValueq\0~\0	L\0updateByq\0~\0	L\0\nupdateTimeq\0~\0xpt\0adminsr\0java.util.Datehj�KYt\0\0xpw\0\0{՗Ƹxpt\0\0pppt\01t\00/15 * * * * ?t\0ryTask.ryParams(\'ry\')t\0DEFAULTsr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0系统默认（有参）t\03t\01x\0'),('RuoyiScheduler','TASK_CLASS_NAME3','DEFAULT',NULL,'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution','0','1','0','0','�\�\0sr\0org.quartz.JobDataMap���迩�\�\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�\�\��\�](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap\�.�(v\n\�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0TASK_PROPERTIESsr\0com.ruoyi.quartz.domain.SysJob\0\0\0\0\0\0\0\0L\0\nconcurrentt\0Ljava/lang/String;L\0cronExpressionq\0~\0	L\0invokeTargetq\0~\0	L\0jobGroupq\0~\0	L\0jobIdt\0Ljava/lang/Long;L\0jobNameq\0~\0	L\0\rmisfirePolicyq\0~\0	L\0statusq\0~\0	xr\0\'com.ruoyi.common.core.domain.BaseEntity\0\0\0\0\0\0\0\0L\0createByq\0~\0	L\0\ncreateTimet\0Ljava/util/Date;L\0paramsq\0~\0L\0remarkq\0~\0	L\0searchValueq\0~\0	L\0updateByq\0~\0	L\0\nupdateTimeq\0~\0xpt\0adminsr\0java.util.Datehj�KYt\0\0xpw\0\0{՗Ƹxpt\0\0pppt\01t\00/20 * * * * ?t\08ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)t\0DEFAULTsr\0java.lang.Long;�\�̏#\�\0J\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0\0\0\0\0t\0系统默认（多参）t\03t\01x\0');
/*!40000 ALTER TABLE `qrtz_job_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_locks`
--

DROP TABLE IF EXISTS `qrtz_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='存储的悲观锁信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_locks`
--

LOCK TABLES `qrtz_locks` WRITE;
/*!40000 ALTER TABLE `qrtz_locks` DISABLE KEYS */;
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler','STATE_ACCESS'),('RuoyiScheduler','TRIGGER_ACCESS');
/*!40000 ALTER TABLE `qrtz_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_paused_trigger_grps`
--

DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='暂停的触发器表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_paused_trigger_grps`
--

LOCK TABLES `qrtz_paused_trigger_grps` WRITE;
/*!40000 ALTER TABLE `qrtz_paused_trigger_grps` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_paused_trigger_grps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_scheduler_state`
--

DROP TABLE IF EXISTS `qrtz_scheduler_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='调度器状态表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_scheduler_state`
--

LOCK TABLES `qrtz_scheduler_state` WRITE;
/*!40000 ALTER TABLE `qrtz_scheduler_state` DISABLE KEYS */;
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler','USER-20180709JW1632541016607',1632541246741,15000);
/*!40000 ALTER TABLE `qrtz_scheduler_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_simple_triggers`
--

DROP TABLE IF EXISTS `qrtz_simple_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='简单触发器的信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_simple_triggers`
--

LOCK TABLES `qrtz_simple_triggers` WRITE;
/*!40000 ALTER TABLE `qrtz_simple_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_simple_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_simprop_triggers`
--

DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='同步机制的行锁表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_simprop_triggers`
--

LOCK TABLES `qrtz_simprop_triggers` WRITE;
/*!40000 ALTER TABLE `qrtz_simprop_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qrtz_simprop_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrtz_triggers`
--

DROP TABLE IF EXISTS `qrtz_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='触发器详细信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrtz_triggers`
--

LOCK TABLES `qrtz_triggers` WRITE;
/*!40000 ALTER TABLE `qrtz_triggers` DISABLE KEYS */;
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler','TASK_CLASS_NAME1','DEFAULT','TASK_CLASS_NAME1','DEFAULT',NULL,1632541020000,-1,5,'PAUSED','CRON',1632541016000,0,NULL,2,''),('RuoyiScheduler','TASK_CLASS_NAME2','DEFAULT','TASK_CLASS_NAME2','DEFAULT',NULL,1632541020000,-1,5,'PAUSED','CRON',1632541016000,0,NULL,2,''),('RuoyiScheduler','TASK_CLASS_NAME3','DEFAULT','TASK_CLASS_NAME3','DEFAULT',NULL,1632541020000,-1,5,'PAUSED','CRON',1632541016000,0,NULL,2,'');
/*!40000 ALTER TABLE `qrtz_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'主框架页-默认皮肤样式名称','sys.index.skinName','skin-blue','Y','admin','2021-09-12 00:01:39','',NULL,'蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow'),(2,'用户管理-账号初始密码','sys.user.initPassword','123456','Y','admin','2021-09-12 00:01:39','',NULL,'初始化密码 123456'),(3,'主框架页-侧边栏主题','sys.index.sideTheme','theme-dark','Y','admin','2021-09-12 00:01:39','',NULL,'深色主题theme-dark，浅色主题theme-light'),(4,'账号自助-验证码开关','sys.account.captchaOnOff','true','N','admin','2021-09-12 00:01:39','admin','2021-09-24 16:08:50','是否开启验证码功能（true开启，false关闭）'),(5,'账号自助-是否开启用户注册功能','sys.account.registerUser','false','Y','admin','2021-09-12 00:01:39','',NULL,'是否开启注册用户功能（true开启，false关闭）');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES (100,0,'0','若依科技',0,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(101,100,'0,100','深圳总公司',1,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(102,100,'0,100','长沙分公司',2,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(103,101,'0,100,101','研发部门',1,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(104,101,'0,100,101','市场部门',2,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(105,101,'0,100,101','测试部门',3,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(106,101,'0,100,101','财务部门',4,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(107,101,'0,100,101','运维部门',5,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(108,102,'0,100,102','市场部门',1,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL),(109,102,'0,100,102','财务部门',2,'若依','15888888888','ry@qq.com','0','0','admin','2021-09-12 00:01:33','',NULL);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES (1,1,'男','0','sys_user_sex','','','Y','0','admin','2021-09-12 00:01:38','',NULL,'性别男'),(2,2,'女','1','sys_user_sex','','','N','0','admin','2021-09-12 00:01:38','',NULL,'性别女'),(3,3,'未知','2','sys_user_sex','','','N','0','admin','2021-09-12 00:01:38','',NULL,'性别未知'),(4,1,'显示','0','sys_show_hide','','primary','Y','0','admin','2021-09-12 00:01:38','',NULL,'显示菜单'),(5,2,'隐藏','1','sys_show_hide','','danger','N','0','admin','2021-09-12 00:01:38','',NULL,'隐藏菜单'),(6,1,'正常','0','sys_normal_disable','','primary','Y','0','admin','2021-09-12 00:01:38','',NULL,'正常状态'),(7,2,'停用','1','sys_normal_disable','','danger','N','0','admin','2021-09-12 00:01:38','',NULL,'停用状态'),(8,1,'正常','0','sys_job_status','','primary','Y','0','admin','2021-09-12 00:01:38','',NULL,'正常状态'),(9,2,'暂停','1','sys_job_status','','danger','N','0','admin','2021-09-12 00:01:38','',NULL,'停用状态'),(10,1,'默认','DEFAULT','sys_job_group','','','Y','0','admin','2021-09-12 00:01:38','',NULL,'默认分组'),(11,2,'系统','SYSTEM','sys_job_group','','','N','0','admin','2021-09-12 00:01:38','',NULL,'系统分组'),(12,1,'是','Y','sys_yes_no','','primary','Y','0','admin','2021-09-12 00:01:38','',NULL,'系统默认是'),(13,2,'否','N','sys_yes_no','','danger','N','0','admin','2021-09-12 00:01:38','',NULL,'系统默认否'),(14,1,'通知','1','sys_notice_type','','warning','Y','0','admin','2021-09-12 00:01:38','',NULL,'通知'),(15,2,'公告','2','sys_notice_type','','success','N','0','admin','2021-09-12 00:01:38','',NULL,'公告'),(16,1,'正常','0','sys_notice_status','','primary','Y','0','admin','2021-09-12 00:01:38','',NULL,'正常状态'),(17,2,'关闭','1','sys_notice_status','','danger','N','0','admin','2021-09-12 00:01:38','',NULL,'关闭状态'),(18,1,'新增','1','sys_oper_type','','info','N','0','admin','2021-09-12 00:01:39','',NULL,'新增操作'),(19,2,'修改','2','sys_oper_type','','info','N','0','admin','2021-09-12 00:01:39','',NULL,'修改操作'),(20,3,'删除','3','sys_oper_type','','danger','N','0','admin','2021-09-12 00:01:39','',NULL,'删除操作'),(21,4,'授权','4','sys_oper_type','','primary','N','0','admin','2021-09-12 00:01:39','',NULL,'授权操作'),(22,5,'导出','5','sys_oper_type','','warning','N','0','admin','2021-09-12 00:01:39','',NULL,'导出操作'),(23,6,'导入','6','sys_oper_type','','warning','N','0','admin','2021-09-12 00:01:39','',NULL,'导入操作'),(24,7,'强退','7','sys_oper_type','','danger','N','0','admin','2021-09-12 00:01:39','',NULL,'强退操作'),(25,8,'生成代码','8','sys_oper_type','','warning','N','0','admin','2021-09-12 00:01:39','',NULL,'生成操作'),(26,9,'清空数据','9','sys_oper_type','','danger','N','0','admin','2021-09-12 00:01:39','',NULL,'清空操作'),(27,1,'成功','0','sys_common_status','','primary','N','0','admin','2021-09-12 00:01:39','',NULL,'正常状态'),(28,2,'失败','1','sys_common_status','','danger','N','0','admin','2021-09-12 00:01:39','',NULL,'停用状态'),(100,0,'待售','0','fm_sell_status',NULL,'default','N','0','admin','2021-09-15 23:16:42','admin','2021-09-15 23:17:36',NULL),(101,1,'已售','1','fm_sell_status',NULL,'default','N','0','admin','2021-09-15 23:17:05','',NULL,NULL),(102,0,' 商品','1','fm_good_type',NULL,'default','N','0','admin','2021-09-24 15:10:10','',NULL,NULL),(103,0,'服务','2','fm_good_type',NULL,'default','N','0','admin','2021-09-24 15:10:28','',NULL,NULL),(104,0,'格位','1','fm_cell_type',NULL,'default','N','0','admin','2021-09-24 15:35:58','',NULL,NULL),(105,0,'墓地','2','fm_cell_type',NULL,'default','N','0','admin','2021-09-24 15:36:09','',NULL,NULL),(106,0,'迁入','1','fm_cell_op',NULL,'default','N','0','admin','2021-09-24 17:15:31','',NULL,NULL),(107,0,'迁出','2','fm_cell_op',NULL,'default','N','0','admin','2021-09-24 17:15:41','',NULL,NULL);
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` VALUES (1,'用户性别','sys_user_sex','0','admin','2021-09-12 00:01:38','',NULL,'用户性别列表'),(2,'菜单状态','sys_show_hide','0','admin','2021-09-12 00:01:38','',NULL,'菜单状态列表'),(3,'系统开关','sys_normal_disable','0','admin','2021-09-12 00:01:38','',NULL,'系统开关列表'),(4,'任务状态','sys_job_status','0','admin','2021-09-12 00:01:38','',NULL,'任务状态列表'),(5,'任务分组','sys_job_group','0','admin','2021-09-12 00:01:38','',NULL,'任务分组列表'),(6,'系统是否','sys_yes_no','0','admin','2021-09-12 00:01:38','',NULL,'系统是否列表'),(7,'通知类型','sys_notice_type','0','admin','2021-09-12 00:01:38','',NULL,'通知类型列表'),(8,'通知状态','sys_notice_status','0','admin','2021-09-12 00:01:38','',NULL,'通知状态列表'),(9,'操作类型','sys_oper_type','0','admin','2021-09-12 00:01:38','',NULL,'操作类型列表'),(10,'系统状态','sys_common_status','0','admin','2021-09-12 00:01:38','',NULL,'登录状态列表'),(100,'销售状态','fm_sell_status','0','admin','2021-09-15 23:14:55','',NULL,NULL),(101,'商品类型','fm_good_type','0','admin','2021-09-15 23:27:40','',NULL,NULL),(102,'格位类型','fm_cell_type','0','admin','2021-09-24 15:34:34','admin','2021-09-24 15:34:57',NULL),(103,'格位操作','fm_cell_op','0','admin','2021-09-24 17:15:09','',NULL,NULL);
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_job`
--

DROP TABLE IF EXISTS `sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务调度表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_job`
--

LOCK TABLES `sys_job` WRITE;
/*!40000 ALTER TABLE `sys_job` DISABLE KEYS */;
INSERT INTO `sys_job` VALUES (1,'系统默认（无参）','DEFAULT','ryTask.ryNoParams','0/10 * * * * ?','3','1','1','admin','2021-09-12 00:01:39','',NULL,''),(2,'系统默认（有参）','DEFAULT','ryTask.ryParams(\'ry\')','0/15 * * * * ?','3','1','1','admin','2021-09-12 00:01:39','',NULL,''),(3,'系统默认（多参）','DEFAULT','ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)','0/20 * * * * ?','3','1','1','admin','2021-09-12 00:01:39','',NULL,'');
/*!40000 ALTER TABLE `sys_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_job_log`
--

DROP TABLE IF EXISTS `sys_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务调度日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_job_log`
--

LOCK TABLES `sys_job_log` WRITE;
/*!40000 ALTER TABLE `sys_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_logininfor`
--

DROP TABLE IF EXISTS `sys_logininfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统访问记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_logininfor`
--

LOCK TABLES `sys_logininfor` WRITE;
/*!40000 ALTER TABLE `sys_logininfor` DISABLE KEYS */;
INSERT INTO `sys_logininfor` VALUES (100,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','用户不存在/密码错误','2021-09-12 01:58:33'),(101,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','验证码错误','2021-09-12 01:58:39'),(102,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','用户不存在/密码错误','2021-09-12 01:58:46'),(103,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-12 02:00:10'),(104,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','用户不存在/密码错误','2021-09-12 16:02:19'),(105,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','验证码错误','2021-09-12 16:02:26'),(106,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-12 16:02:30'),(107,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','验证码错误','2021-09-12 18:13:50'),(108,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-12 18:13:57'),(109,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-13 00:19:11'),(110,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-13 09:59:02'),(111,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','1','验证码错误','2021-09-15 22:51:55'),(112,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-15 22:51:58'),(113,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-16 00:14:26'),(114,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-16 00:44:08'),(115,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-16 00:44:18'),(116,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-16 21:44:44'),(117,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-17 00:20:32'),(118,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-17 01:24:05'),(119,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-17 16:02:30'),(120,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-17 16:43:36'),(121,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-17 23:58:18'),(122,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-19 16:54:32'),(123,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-24 14:51:18'),(124,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-24 17:05:36'),(125,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-24 18:13:43'),(126,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-24 22:54:11'),(127,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-24 23:40:16'),(128,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 10:35:33'),(129,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 10:40:50'),(130,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 10:40:54'),(131,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 10:42:05'),(132,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 11:01:30'),(133,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 11:01:37'),(134,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 11:04:30'),(135,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 11:04:37'),(136,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 11:08:25'),(137,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 11:08:30'),(138,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 11:09:26'),(139,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 11:09:32'),(140,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 11:14:18'),(141,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 11:14:24'),(142,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','退出成功','2021-09-25 11:15:09'),(143,'admin','127.0.0.1','内网IP','Chrome 9','Windows 7','0','登录成功','2021-09-25 11:15:15');
/*!40000 ALTER TABLE `sys_logininfor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'系统管理',0,1,'system',NULL,'',1,0,'M','0','0','','system','admin','2021-09-12 00:01:34','',NULL,'系统管理目录'),(2,'系统监控',0,2,'monitor',NULL,'',1,0,'M','0','0','','monitor','admin','2021-09-12 00:01:34','',NULL,'系统监控目录'),(3,'系统工具',0,3,'tool',NULL,'',1,0,'M','0','0','','tool','admin','2021-09-12 00:01:34','',NULL,'系统工具目录'),(4,'墓园业务',0,4,'fm',NULL,'',1,0,'M','0','0','','guide','admin','2021-09-12 00:01:34','admin','2021-09-19 16:57:15','墓园业务目录'),(100,'用户管理',1,1,'user','system/user/index','',1,0,'C','0','0','system:user:list','user','admin','2021-09-12 00:01:34','',NULL,'用户管理菜单'),(101,'角色管理',1,2,'role','system/role/index','',1,0,'C','0','0','system:role:list','peoples','admin','2021-09-12 00:01:34','',NULL,'角色管理菜单'),(102,'菜单管理',1,3,'menu','system/menu/index','',1,0,'C','0','0','system:menu:list','tree-table','admin','2021-09-12 00:01:34','',NULL,'菜单管理菜单'),(103,'部门管理',1,4,'dept','system/dept/index','',1,0,'C','0','0','system:dept:list','tree','admin','2021-09-12 00:01:34','',NULL,'部门管理菜单'),(104,'岗位管理',1,5,'post','system/post/index','',1,0,'C','0','0','system:post:list','post','admin','2021-09-12 00:01:34','',NULL,'岗位管理菜单'),(105,'字典管理',1,6,'dict','system/dict/index','',1,0,'C','0','0','system:dict:list','dict','admin','2021-09-12 00:01:34','',NULL,'字典管理菜单'),(106,'参数设置',1,7,'config','system/config/index','',1,0,'C','0','0','system:config:list','edit','admin','2021-09-12 00:01:34','',NULL,'参数设置菜单'),(107,'通知公告',1,8,'notice','system/notice/index','',1,0,'C','0','0','system:notice:list','message','admin','2021-09-12 00:01:34','',NULL,'通知公告菜单'),(108,'日志管理',1,9,'log','','',1,0,'M','0','0','','log','admin','2021-09-12 00:01:34','',NULL,'日志管理菜单'),(109,'在线用户',2,1,'online','monitor/online/index','',1,0,'C','0','0','monitor:online:list','online','admin','2021-09-12 00:01:34','',NULL,'在线用户菜单'),(110,'定时任务',2,2,'job','monitor/job/index','',1,0,'C','0','0','monitor:job:list','job','admin','2021-09-12 00:01:34','',NULL,'定时任务菜单'),(111,'数据监控',2,3,'druid','monitor/druid/index','',1,0,'C','0','0','monitor:druid:list','druid','admin','2021-09-12 00:01:34','',NULL,'数据监控菜单'),(112,'服务监控',2,4,'server','monitor/server/index','',1,0,'C','0','0','monitor:server:list','server','admin','2021-09-12 00:01:34','',NULL,'服务监控菜单'),(113,'缓存监控',2,5,'cache','monitor/cache/index','',1,0,'C','0','0','monitor:cache:list','redis','admin','2021-09-12 00:01:34','',NULL,'缓存监控菜单'),(114,'表单构建',3,1,'build','tool/build/index','',1,0,'C','0','0','tool:build:list','build','admin','2021-09-12 00:01:34','',NULL,'表单构建菜单'),(115,'代码生成',3,2,'gen','tool/gen/index','',1,0,'C','0','0','tool:gen:list','code','admin','2021-09-12 00:01:34','',NULL,'代码生成菜单'),(116,'系统接口',3,3,'swagger','tool/swagger/index','',1,0,'C','0','0','tool:swagger:list','swagger','admin','2021-09-12 00:01:34','',NULL,'系统接口菜单'),(500,'操作日志',108,1,'operlog','monitor/operlog/index','',1,0,'C','0','0','monitor:operlog:list','form','admin','2021-09-12 00:01:34','',NULL,'操作日志菜单'),(501,'登录日志',108,2,'logininfor','monitor/logininfor/index','',1,0,'C','0','0','monitor:logininfor:list','logininfor','admin','2021-09-12 00:01:34','',NULL,'登录日志菜单'),(1001,'用户查询',100,1,'','','',1,0,'F','0','0','system:user:query','#','admin','2021-09-12 00:01:34','',NULL,''),(1002,'用户新增',100,2,'','','',1,0,'F','0','0','system:user:add','#','admin','2021-09-12 00:01:34','',NULL,''),(1003,'用户修改',100,3,'','','',1,0,'F','0','0','system:user:edit','#','admin','2021-09-12 00:01:34','',NULL,''),(1004,'用户删除',100,4,'','','',1,0,'F','0','0','system:user:remove','#','admin','2021-09-12 00:01:34','',NULL,''),(1005,'用户导出',100,5,'','','',1,0,'F','0','0','system:user:export','#','admin','2021-09-12 00:01:34','',NULL,''),(1006,'用户导入',100,6,'','','',1,0,'F','0','0','system:user:import','#','admin','2021-09-12 00:01:34','',NULL,''),(1007,'重置密码',100,7,'','','',1,0,'F','0','0','system:user:resetPwd','#','admin','2021-09-12 00:01:34','',NULL,''),(1008,'角色查询',101,1,'','','',1,0,'F','0','0','system:role:query','#','admin','2021-09-12 00:01:34','',NULL,''),(1009,'角色新增',101,2,'','','',1,0,'F','0','0','system:role:add','#','admin','2021-09-12 00:01:34','',NULL,''),(1010,'角色修改',101,3,'','','',1,0,'F','0','0','system:role:edit','#','admin','2021-09-12 00:01:34','',NULL,''),(1011,'角色删除',101,4,'','','',1,0,'F','0','0','system:role:remove','#','admin','2021-09-12 00:01:34','',NULL,''),(1012,'角色导出',101,5,'','','',1,0,'F','0','0','system:role:export','#','admin','2021-09-12 00:01:34','',NULL,''),(1013,'菜单查询',102,1,'','','',1,0,'F','0','0','system:menu:query','#','admin','2021-09-12 00:01:34','',NULL,''),(1014,'菜单新增',102,2,'','','',1,0,'F','0','0','system:menu:add','#','admin','2021-09-12 00:01:34','',NULL,''),(1015,'菜单修改',102,3,'','','',1,0,'F','0','0','system:menu:edit','#','admin','2021-09-12 00:01:34','',NULL,''),(1016,'菜单删除',102,4,'','','',1,0,'F','0','0','system:menu:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1017,'部门查询',103,1,'','','',1,0,'F','0','0','system:dept:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1018,'部门新增',103,2,'','','',1,0,'F','0','0','system:dept:add','#','admin','2021-09-12 00:01:35','',NULL,''),(1019,'部门修改',103,3,'','','',1,0,'F','0','0','system:dept:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1020,'部门删除',103,4,'','','',1,0,'F','0','0','system:dept:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1021,'岗位查询',104,1,'','','',1,0,'F','0','0','system:post:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1022,'岗位新增',104,2,'','','',1,0,'F','0','0','system:post:add','#','admin','2021-09-12 00:01:35','',NULL,''),(1023,'岗位修改',104,3,'','','',1,0,'F','0','0','system:post:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1024,'岗位删除',104,4,'','','',1,0,'F','0','0','system:post:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1025,'岗位导出',104,5,'','','',1,0,'F','0','0','system:post:export','#','admin','2021-09-12 00:01:35','',NULL,''),(1026,'字典查询',105,1,'#','','',1,0,'F','0','0','system:dict:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1027,'字典新增',105,2,'#','','',1,0,'F','0','0','system:dict:add','#','admin','2021-09-12 00:01:35','',NULL,''),(1028,'字典修改',105,3,'#','','',1,0,'F','0','0','system:dict:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1029,'字典删除',105,4,'#','','',1,0,'F','0','0','system:dict:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1030,'字典导出',105,5,'#','','',1,0,'F','0','0','system:dict:export','#','admin','2021-09-12 00:01:35','',NULL,''),(1031,'参数查询',106,1,'#','','',1,0,'F','0','0','system:config:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1032,'参数新增',106,2,'#','','',1,0,'F','0','0','system:config:add','#','admin','2021-09-12 00:01:35','',NULL,''),(1033,'参数修改',106,3,'#','','',1,0,'F','0','0','system:config:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1034,'参数删除',106,4,'#','','',1,0,'F','0','0','system:config:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1035,'参数导出',106,5,'#','','',1,0,'F','0','0','system:config:export','#','admin','2021-09-12 00:01:35','',NULL,''),(1036,'公告查询',107,1,'#','','',1,0,'F','0','0','system:notice:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1037,'公告新增',107,2,'#','','',1,0,'F','0','0','system:notice:add','#','admin','2021-09-12 00:01:35','',NULL,''),(1038,'公告修改',107,3,'#','','',1,0,'F','0','0','system:notice:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1039,'公告删除',107,4,'#','','',1,0,'F','0','0','system:notice:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1040,'操作查询',500,1,'#','','',1,0,'F','0','0','monitor:operlog:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1041,'操作删除',500,2,'#','','',1,0,'F','0','0','monitor:operlog:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1042,'日志导出',500,4,'#','','',1,0,'F','0','0','monitor:operlog:export','#','admin','2021-09-12 00:01:35','',NULL,''),(1043,'登录查询',501,1,'#','','',1,0,'F','0','0','monitor:logininfor:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1044,'登录删除',501,2,'#','','',1,0,'F','0','0','monitor:logininfor:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1045,'日志导出',501,3,'#','','',1,0,'F','0','0','monitor:logininfor:export','#','admin','2021-09-12 00:01:35','',NULL,''),(1046,'在线查询',109,1,'#','','',1,0,'F','0','0','monitor:online:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1047,'批量强退',109,2,'#','','',1,0,'F','0','0','monitor:online:batchLogout','#','admin','2021-09-12 00:01:35','',NULL,''),(1048,'单条强退',109,3,'#','','',1,0,'F','0','0','monitor:online:forceLogout','#','admin','2021-09-12 00:01:35','',NULL,''),(1049,'任务查询',110,1,'#','','',1,0,'F','0','0','monitor:job:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1050,'任务新增',110,2,'#','','',1,0,'F','0','0','monitor:job:add','#','admin','2021-09-12 00:01:35','',NULL,''),(1051,'任务修改',110,3,'#','','',1,0,'F','0','0','monitor:job:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1052,'任务删除',110,4,'#','','',1,0,'F','0','0','monitor:job:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1053,'状态修改',110,5,'#','','',1,0,'F','0','0','monitor:job:changeStatus','#','admin','2021-09-12 00:01:35','',NULL,''),(1054,'任务导出',110,7,'#','','',1,0,'F','0','0','monitor:job:export','#','admin','2021-09-12 00:01:35','',NULL,''),(1055,'生成查询',115,1,'#','','',1,0,'F','0','0','tool:gen:query','#','admin','2021-09-12 00:01:35','',NULL,''),(1056,'生成修改',115,2,'#','','',1,0,'F','0','0','tool:gen:edit','#','admin','2021-09-12 00:01:35','',NULL,''),(1057,'生成删除',115,3,'#','','',1,0,'F','0','0','tool:gen:remove','#','admin','2021-09-12 00:01:35','',NULL,''),(1058,'导入代码',115,2,'#','','',1,0,'F','0','0','tool:gen:import','#','admin','2021-09-12 00:01:35','',NULL,''),(1059,'预览代码',115,4,'#','','',1,0,'F','0','0','tool:gen:preview','#','admin','2021-09-12 00:01:35','',NULL,''),(1060,'生成代码',115,5,'#','','',1,0,'F','0','0','tool:gen:code','#','admin','2021-09-12 00:01:35','',NULL,''),(2096,'用户信息',4,1,'custinfo','fm/custinfo/index',NULL,1,0,'C','0','0','fm:custinfo:list','#','admin','2021-09-16 22:02:54','admin','2021-09-18 00:56:42','用户信息菜单'),(2097,'用户信息查询',2096,1,'#','',NULL,1,0,'F','0','0','fm:custinfo:query','#','admin','2021-09-16 22:02:54','',NULL,''),(2098,'用户信息新增',2096,2,'#','',NULL,1,0,'F','0','0','fm:custinfo:add','#','admin','2021-09-16 22:02:54','',NULL,''),(2099,'用户信息修改',2096,3,'#','',NULL,1,0,'F','0','0','fm:custinfo:edit','#','admin','2021-09-16 22:02:55','',NULL,''),(2100,'用户信息删除',2096,4,'#','',NULL,1,0,'F','0','0','fm:custinfo:remove','#','admin','2021-09-16 22:02:55','',NULL,''),(2101,'用户信息导出',2096,5,'#','',NULL,1,0,'F','0','0','fm:custinfo:export','#','admin','2021-09-16 22:02:55','',NULL,''),(2102,'逝者信息',4,1,'dead','fm/dead/index',NULL,1,0,'C','0','0','fm:dead:list','#','admin','2021-09-16 22:02:57','',NULL,'逝者信息菜单'),(2103,'逝者信息查询',2102,1,'#','',NULL,1,0,'F','0','0','fm:dead:query','#','admin','2021-09-16 22:02:57','',NULL,''),(2104,'逝者信息新增',2102,2,'#','',NULL,1,0,'F','0','0','fm:dead:add','#','admin','2021-09-16 22:02:57','',NULL,''),(2105,'逝者信息修改',2102,3,'#','',NULL,1,0,'F','0','0','fm:dead:edit','#','admin','2021-09-16 22:02:57','',NULL,''),(2106,'逝者信息删除',2102,4,'#','',NULL,1,0,'F','0','0','fm:dead:remove','#','admin','2021-09-16 22:02:57','',NULL,''),(2107,'逝者信息导出',2102,5,'#','',NULL,1,0,'F','0','0','fm:dead:export','#','admin','2021-09-16 22:02:57','',NULL,''),(2108,'商品销售',4,1,'goodOrder','fm/goodOrder/index',NULL,1,0,'C','0','0','fm:goodOrder:list','#','admin','2021-09-16 22:03:16','',NULL,'商品销售菜单'),(2109,'商品销售查询',2108,1,'#','',NULL,1,0,'F','0','0','fm:goodOrder:query','#','admin','2021-09-16 22:03:17','',NULL,''),(2110,'商品销售新增',2108,2,'#','',NULL,1,0,'F','0','0','fm:goodOrder:add','#','admin','2021-09-16 22:03:17','',NULL,''),(2111,'商品销售修改',2108,3,'#','',NULL,1,0,'F','0','0','fm:goodOrder:edit','#','admin','2021-09-16 22:03:17','',NULL,''),(2112,'商品销售删除',2108,4,'#','',NULL,1,0,'F','0','0','fm:goodOrder:remove','#','admin','2021-09-16 22:03:17','',NULL,''),(2113,'商品销售导出',2108,5,'#','',NULL,1,0,'F','0','0','fm:goodOrder:export','#','admin','2021-09-16 22:03:17','',NULL,''),(2114,'商品',4,1,'goods','fm/goods/index',NULL,1,0,'C','0','0','fm:goods:list','#','admin','2021-09-16 22:03:20','',NULL,'商品菜单'),(2115,'商品查询',2114,1,'#','',NULL,1,0,'F','0','0','fm:goods:query','#','admin','2021-09-16 22:03:20','',NULL,''),(2116,'商品新增',2114,2,'#','',NULL,1,0,'F','0','0','fm:goods:add','#','admin','2021-09-16 22:03:20','',NULL,''),(2117,'商品修改',2114,3,'#','',NULL,1,0,'F','0','0','fm:goods:edit','#','admin','2021-09-16 22:03:20','',NULL,''),(2118,'商品删除',2114,4,'#','',NULL,1,0,'F','0','0','fm:goods:remove','#','admin','2021-09-16 22:03:20','',NULL,''),(2119,'商品导出',2114,5,'#','',NULL,1,0,'F','0','0','fm:goods:export','#','admin','2021-09-16 22:03:20','',NULL,''),(2120,'出入土订单',4,1,'inOutOrder','fm/inOutOrder/index',NULL,1,0,'C','0','0','fm:inOutOrder:list','#','admin','2021-09-16 22:03:22','',NULL,'出入土订单菜单'),(2121,'出入土订单查询',2120,1,'#','',NULL,1,0,'F','0','0','fm:inOutOrder:query','#','admin','2021-09-16 22:03:22','',NULL,''),(2122,'出入土订单新增',2120,2,'#','',NULL,1,0,'F','0','0','fm:inOutOrder:add','#','admin','2021-09-16 22:03:22','',NULL,''),(2123,'出入土订单修改',2120,3,'#','',NULL,1,0,'F','0','0','fm:inOutOrder:edit','#','admin','2021-09-16 22:03:23','',NULL,''),(2124,'出入土订单删除',2120,4,'#','',NULL,1,0,'F','0','0','fm:inOutOrder:remove','#','admin','2021-09-16 22:03:23','',NULL,''),(2125,'出入土订单导出',2120,5,'#','',NULL,1,0,'F','0','0','fm:inOutOrder:export','#','admin','2021-09-16 22:03:23','',NULL,''),(2126,'工程管理',4,1,'prj','fm/prj/index',NULL,1,0,'C','0','0','fm:prj:list','#','admin','2021-09-16 22:03:27','',NULL,'工程管理菜单'),(2127,'工程管理查询',2126,1,'#','',NULL,1,0,'F','0','0','fm:prj:query','#','admin','2021-09-16 22:03:27','',NULL,''),(2128,'工程管理新增',2126,2,'#','',NULL,1,0,'F','0','0','fm:prj:add','#','admin','2021-09-16 22:03:27','',NULL,''),(2129,'工程管理修改',2126,3,'#','',NULL,1,0,'F','0','0','fm:prj:edit','#','admin','2021-09-16 22:03:27','',NULL,''),(2130,'工程管理删除',2126,4,'#','',NULL,1,0,'F','0','0','fm:prj:remove','#','admin','2021-09-16 22:03:27','',NULL,''),(2131,'工程管理导出',2126,5,'#','',NULL,1,0,'F','0','0','fm:prj:export','#','admin','2021-09-16 22:03:27','',NULL,''),(2132,'格位',4,1,'cell','fm/cell/index',NULL,1,0,'C','0','0','fm:cell:list','#','admin','2021-09-16 22:03:31','admin','2021-09-19 16:58:19','格位菜单'),(2133,'格位查询',2132,1,'#','',NULL,1,0,'F','0','0','fm:cell:query','#','admin','2021-09-16 22:03:31','',NULL,''),(2134,'格位新增',2132,2,'#','',NULL,1,0,'F','0','0','fm:cell:add','#','admin','2021-09-16 22:03:31','',NULL,''),(2135,'格位修改',2132,3,'#','',NULL,1,0,'F','0','0','fm:cell:edit','#','admin','2021-09-16 22:03:31','',NULL,''),(2136,'格位删除',2132,4,'#','',NULL,1,0,'F','0','0','fm:cell:remove','#','admin','2021-09-16 22:03:31','',NULL,''),(2137,'格位导出',2132,5,'#','',NULL,1,0,'F','0','0','fm:cell:export','#','admin','2021-09-16 22:03:31','',NULL,''),(2138,'格位销售',4,1,'cellOrder','fm/cellOrder/index',NULL,1,0,'C','0','0','fm:cellOrder:list','#','admin','2021-09-16 22:03:35','admin','2021-09-19 16:58:29','格位销售菜单'),(2139,'格位销售查询',2138,1,'#','',NULL,1,0,'F','0','0','fm:cellOrder:query','#','admin','2021-09-16 22:03:35','',NULL,''),(2140,'格位销售新增',2138,2,'#','',NULL,1,0,'F','0','0','fm:cellOrder:add','#','admin','2021-09-16 22:03:35','',NULL,''),(2141,'格位销售修改',2138,3,'#','',NULL,1,0,'F','0','0','fm:cellOrder:edit','#','admin','2021-09-16 22:03:36','',NULL,''),(2142,'格位销售删除',2138,4,'#','',NULL,1,0,'F','0','0','fm:cellOrder:remove','#','admin','2021-09-16 22:03:36','',NULL,''),(2143,'格位销售导出',2138,5,'#','',NULL,1,0,'F','0','0','fm:cellOrder:export','#','admin','2021-09-16 22:03:36','',NULL,'');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_notice`
--

DROP TABLE IF EXISTS `sys_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='通知公告表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_notice`
--

LOCK TABLES `sys_notice` WRITE;
/*!40000 ALTER TABLE `sys_notice` DISABLE KEYS */;
INSERT INTO `sys_notice` VALUES (1,'温馨提醒：2018-07-01 若依新版本发布啦','2','新版本内容','0','admin','2021-09-12 00:01:39','',NULL,'管理员'),(2,'维护通知：2018-07-01 若依系统凌晨维护','1','维护内容','0','admin','2021-09-12 00:01:39','',NULL,'管理员');
/*!40000 ALTER TABLE `sys_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_oper_log`
--

DROP TABLE IF EXISTS `sys_oper_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='操作日志记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_oper_log`
--

LOCK TABLES `sys_oper_log` WRITE;
/*!40000 ALTER TABLE `sys_oper_log` DISABLE KEYS */;
INSERT INTO `sys_oper_log` VALUES (100,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','fm_prj,fm_order_goods_detail,fm_good_type,fm_dead,fm_custinfo,fm_cell,fm_goods,fm_cell_sell_order,fm_put_in_cell,fm_good_sell_order','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-12 16:13:37'),(101,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','sys_job_log,sys_logininfor,sys_config,sys_notice,sys_job,sys_user_post,sys_oper_log,sys_dict_data,sys_dict_type,sys_role_dept','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-12 16:13:44'),(102,'代码生成',3,'com.ruoyi.generator.controller.GenController.remove()','DELETE',1,'admin',NULL,'/tool/gen/11,12,13,14,15,16,17,18,19,20','127.0.0.1','内网IP','{tableIds=11,12,13,14,15,16,17,18,19,20}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-12 16:14:28'),(103,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:14:47'),(104,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:33:10'),(105,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:33:16'),(106,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:33:23'),(107,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:33:29'),(108,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:34:59'),(109,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:37:41'),(110,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:38:08'),(111,'代码生成',3,'com.ruoyi.generator.controller.GenController.remove()','DELETE',1,'admin',NULL,'/tool/gen/1,2,3,4,5,6,7,8,9,10','127.0.0.1','内网IP','{tableIds=1,2,3,4,5,6,7,8,9,10}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-12 16:39:24'),(112,'代码生成',6,'com.ruoyi.generator.controller.GenController.importTableSave()','POST',1,'admin',NULL,'/tool/gen/importTable','127.0.0.1','内网IP','fm_prj,fm_order_goods_detail,fm_good_type,fm_dead,fm_custinfo,fm_cell,fm_goods,fm_cell_sell_order,fm_put_in_cell,fm_good_sell_order','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-12 16:39:34'),(113,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-12 16:39:43'),(114,'角色管理',2,'com.ruoyi.web.controller.system.SysRoleController.edit()','PUT',1,'admin',NULL,'/system/role','127.0.0.1','内网IP','{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1631376094000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113,3,114,115,1055,1056,1058,1057,1059,1060,116,4,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,2035,2036,2037,2038,2039,2040,2041],\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-12 18:09:00'),(115,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Double\",\"queryType\":\"EQ\",\"columnType\":\"decimal(16,2)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"create','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-13 00:33:42'),(116,'字典类型',9,'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()','DELETE',1,'admin',NULL,'/system/dict/type/refreshCache','127.0.0.1','内网IP','{}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-13 00:49:39'),(117,'字典类型',9,'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()','DELETE',1,'admin',NULL,'/system/dict/type/refreshCache','127.0.0.1','内网IP','{}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:07:16'),(118,'字典类型',1,'com.ruoyi.web.controller.system.SysDictTypeController.add()','POST',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"dictName\":\"销售状态\",\"params\":{},\"dictType\":\"fm_sell_status\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:14:55'),(119,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"待售\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_sell_status\",\"dictLabel\":\"0\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:16:42'),(120,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"fm_sell_status\",\"dictLabel\":\"已售\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:17:05'),(121,'字典数据',2,'com.ruoyi.web.controller.system.SysDictDataController.edit()','PUT',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"0\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_sell_status\",\"dictLabel\":\"待售\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1631719002000,\"dictCode\":100,\"updateBy\":\"admin\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:17:36'),(122,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631464422000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1631464422000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"updateTime\":1631464422000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"updateTime\":1631464422000,\"sort\":4,\"list\":true,\"params','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:18:42'),(123,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_cell','127.0.0.1','内网IP','{tableName=fm_cell}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:21:13'),(124,'代码生成',3,'com.ruoyi.generator.controller.GenController.remove()','DELETE',1,'admin',NULL,'/tool/gen/26','127.0.0.1','内网IP','{tableIds=26}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:27:02'),(125,'字典类型',1,'com.ruoyi.web.controller.system.SysDictTypeController.add()','POST',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"dictName\":\"商品类型\",\"params\":{},\"dictType\":\"fm_good_type\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:27:40'),(126,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631719122000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1631719122000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"updateTime\":1631719122000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"updateTime\":1631719122000,\"sort\":4,\"list\":true,\"params','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:30:57'),(127,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":188,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":23,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"CustName\",\"usableColumn\":false,\"columnId\":189,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"custName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"客户姓名\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(24)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":23,\"pk\":false,\"columnName\":\"cust_name\"},{\"capJavaField\":\"Sex\",\"usableColumn\":false,\"columnId\":190,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"sys_user_sex\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"sex\",\"htmlType\":\"select\",\"edit\":true,\"query\":false,\"columnComment\":\"性别\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(1)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":23,\"pk\":false,\"columnName\":\"sex\"},{\"capJavaField\":\"Number\",\"usableColumn\":false,\"columnId\":191,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"number\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"身份证\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"cr','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:32:04'),(128,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":194,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":24,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Deceased\",\"usableColumn\":false,\"columnId\":195,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"deceased\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"逝者姓名\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":24,\"pk\":false,\"columnName\":\"deceased\"},{\"capJavaField\":\"Sex\",\"usableColumn\":false,\"columnId\":196,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"sys_user_sex\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"sex\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"性别\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":24,\"pk\":false,\"columnName\":\"sex\"},{\"capJavaField\":\"Number\",\"usableColumn\":false,\"columnId\":197,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"number\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"身份证号码\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:32:57'),(129,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":210,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":27,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":211,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"商品名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":27,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"GoodType\",\"usableColumn\":false,\"columnId\":212,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"fm_good_type\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"goodType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"商品类型ID\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":27,\"pk\":false,\"columnName\":\"good_type\"},{\"capJavaField\":\"TypeName\",\"usableColumn\":false,\"columnId\":213,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"typeName\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"商品类型\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(45)\",\"createBy\":\"','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:38:28'),(130,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":223,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":224,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"登录ID\",\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateBy\",\"usableColumn\":false,\"columnId\":225,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":29,\"pk\":false,\"columnName\":\"create_by\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":226,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"BETWEEN\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"cre','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:45:56'),(131,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":217,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":218,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\" 商品名称\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":28,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Prize\",\"usableColumn\":false,\"columnId\":219,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"prize\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"单价\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"double\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":28,\"pk\":false,\"columnName\":\"prize\"},{\"capJavaField\":\"Num\",\"usableColumn\":false,\"columnId\":220,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"数量\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":28,\"pk\":fa','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:47:23'),(132,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":217,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1631720843000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":218,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\" 商品名称\",\"updateTime\":1631720843000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Prize\",\"usableColumn\":false,\"columnId\":219,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"prize\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"单价\",\"updateTime\":1631720843000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"double\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":false,\"columnName\":\"prize\"},{\"capJavaField\":\"Num\",\"usableColumn\":false,\"columnId\":220,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"数量\",\"updateTime\":1631720843000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"column','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:49:10'),(133,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":200,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":201,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":202,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"BETWEEN\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"create_time\"},{\"capJavaField\":\"CellId\",\"usableColumn\":false,\"columnId\":203,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cellId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"订单编号\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:53:21'),(134,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-15 23:53:34'),(135,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-15 23:53:40'),(136,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-15 23:53:47'),(137,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-15 23:53:54'),(138,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-15 23:54:01'),(139,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":217,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1631720950000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":218,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\" 商品名称\",\"updateTime\":1631720950000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Prize\",\"usableColumn\":false,\"columnId\":219,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"prize\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"单价\",\"updateTime\":1631720950000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"double\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":false,\"columnName\":\"prize\"},{\"capJavaField\":\"Num\",\"usableColumn\":false,\"columnId\":220,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"数量\",\"updateTime\":1631720950000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"column','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:57:00'),(140,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户ID\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"CustomerName\",\"usableColumn\":false,\"columnId\":178,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"customerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(45)\",\"createBy\":\"','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-15 23:57:31'),(141,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":231,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":232,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"登录id\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"OperatorName\",\"usableColumn\":false,\"columnId\":233,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人姓名\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"operator_name\"},{\"capJavaField\":\"OperatorIdentification\",\"usableColumn\":false,\"columnId\":234,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorIdentification\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人身份证\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:00:30'),(142,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631721451000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"updateTime\":1631721451000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户ID\",\"isQuery\":\"1\",\"updateTime\":1631721451000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"CustomerName\",\"usableColumn\":false,\"columnId\":178,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"customerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户\",\"isQuery\":\"1\",\"updateTime\":1631721451000,\"so','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:00:54'),(143,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":200,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631721201000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":201,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1631721201000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":202,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1631721201000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"BETWEEN\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"create_time\"},{\"capJavaField\":\"CellId\",\"usableColumn\":false,\"columnId\":203,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cellId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"订单编号\",\"isQuery\":\"1\",\"updateTime\":1631721201000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:01:14'),(144,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":231,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1631721630000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":232,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"登录id\",\"updateTime\":1631721630000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"OperatorName\",\"usableColumn\":false,\"columnId\":233,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人姓名\",\"isQuery\":\"1\",\"updateTime\":1631721630000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"operator_name\"},{\"capJavaField\":\"OperatorIdentification\",\"usableColumn\":false,\"columnId\":234,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorIdentification\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComme','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:02:11'),(145,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631721654000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"updateTime\":1631721654000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户ID\",\"isQuery\":\"1\",\"updateTime\":1631721654000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"CustomerName\",\"usableColumn\":false,\"columnId\":178,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"customerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户\",\"isQuery\":\"1\",\"updateTime\":1631721654000,\"so','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:02:31'),(146,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":188,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631719924000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":23,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"CustName\",\"usableColumn\":false,\"columnId\":189,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"custName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"客户姓名\",\"isQuery\":\"1\",\"updateTime\":1631719924000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(24)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":23,\"pk\":false,\"columnName\":\"cust_name\"},{\"capJavaField\":\"Sex\",\"usableColumn\":false,\"columnId\":190,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"sys_user_sex\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"sex\",\"htmlType\":\"select\",\"edit\":true,\"query\":false,\"columnComment\":\"性别\",\"updateTime\":1631719924000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(1)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":23,\"pk\":false,\"columnName\":\"sex\"},{\"capJavaField\":\"Number\",\"usableColumn\":false,\"columnId\":191,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"number\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"身份证\",\"isQuery\":\"1\",\"updateTime\":163171','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:02:41'),(147,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":200,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631721674000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":201,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1631721674000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":202,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1631721674000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"BETWEEN\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"create_time\"},{\"capJavaField\":\"CellId\",\"usableColumn\":false,\"columnId\":203,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"cellId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"订单编号\",\"isQuery\":\"1\",\"updateTime\":1631721674000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:02:52'),(148,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":194,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631719977000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":24,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Deceased\",\"usableColumn\":false,\"columnId\":195,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"deceased\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"逝者姓名\",\"isQuery\":\"1\",\"updateTime\":1631719977000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":24,\"pk\":false,\"columnName\":\"deceased\"},{\"capJavaField\":\"Sex\",\"usableColumn\":false,\"columnId\":196,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"sys_user_sex\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"sex\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"性别\",\"isQuery\":\"1\",\"updateTime\":1631719977000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":24,\"pk\":false,\"columnName\":\"sex\"},{\"capJavaField\":\"Number\",\"usableColumn\":false,\"columnId\":197,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"number\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"身份证号码\",\"isQuery\":\"1\",\"updateTim','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:03:02'),(149,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":210,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631720308000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":27,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":211,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"商品名称\",\"isQuery\":\"1\",\"updateTime\":1631720308000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":27,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"GoodType\",\"usableColumn\":false,\"columnId\":212,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"fm_good_type\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"goodType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"商品类型ID\",\"isQuery\":\"1\",\"updateTime\":1631720308000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":27,\"pk\":false,\"columnName\":\"good_type\"},{\"capJavaField\":\"TypeName\",\"usableColumn\":false,\"columnId\":213,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"typeName\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"商品类型\",\"updateTime\":1631720308000,\"','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:03:13'),(150,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":217,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1631721420000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":218,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\" 商品名称\",\"updateTime\":1631721420000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Prize\",\"usableColumn\":false,\"columnId\":219,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"prize\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"单价\",\"updateTime\":1631721420000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"double\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":28,\"pk\":false,\"columnName\":\"prize\"},{\"capJavaField\":\"Num\",\"usableColumn\":false,\"columnId\":220,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"num\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"数量\",\"updateTime\":1631721420000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"column','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:03:25'),(151,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-16 00:18:47'),(152,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":true,\"subTableName\":\"fm_good_sell_order\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631719857000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1631719857000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"updateTime\":1631719857000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"updateTime\":1631719857000,\"sort\":4,\"l','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 00:44:53'),(153,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-16 00:45:04'),(154,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":223,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631720756000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":224,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"登录ID\",\"updateTime\":1631720756000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateBy\",\"usableColumn\":false,\"columnId\":225,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"updateTime\":1631720756000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":29,\"pk\":false,\"columnName\":\"create_by\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":226,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1631720756000,\"sort\":4,\"list\":true,\"p','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 21:57:36'),(155,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":231,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1631721731000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":232,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"登录id\",\"updateTime\":1631721731000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"OperatorName\",\"usableColumn\":false,\"columnId\":233,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人姓名\",\"isQuery\":\"1\",\"updateTime\":1631721731000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"operator_name\"},{\"capJavaField\":\"OperatorIdentification\",\"usableColumn\":false,\"columnId\":234,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorIdentification\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComme','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-16 21:57:46'),(156,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-16 21:58:15'),(157,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"price\":15.0,\"fmGoodSellOrderList\":[{\"money\":\"10\",\"remark\":\"\",\"params\":{}}],\"name\":\"234\",\"location\":\"234\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,                                       status )           values ( ?,             ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-17 00:21:02'),(158,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"用户信息\",\"params\":{},\"parentId\":3,\"isCache\":\"0\",\"path\":\"custinfo\",\"component\":\"fm/custinfo/index\",\"children\":[],\"createTime\":1631800974000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2096,\"menuType\":\"C\",\"perms\":\"fm:custinfo:list\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-18 00:55:29'),(159,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"用户信息\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"custinfo\",\"component\":\"fm/custinfo/index\",\"children\":[],\"createTime\":1631800974000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2096,\"menuType\":\"C\",\"perms\":\"fm:custinfo:list\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-18 00:56:42'),(160,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"用户信息\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"custinfo\",\"component\":\"fm/custinfo/index\",\"children\":[],\"createTime\":1631800974000,\"isFrame\":\"0\",\"menuId\":2096,\"menuType\":\"C\",\"perms\":\"fm:custinfo:list\",\"status\":\"0\"}','{\"msg\":\"修改菜单\'用户信息\'失败，地址必须以http(s)://开头\",\"code\":500}',0,NULL,'2021-09-19 16:56:59'),(161,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"用户信息\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"custinfo\",\"component\":\"fm/custinfo/index\",\"children\":[],\"createTime\":1631800974000,\"isFrame\":\"0\",\"menuId\":2096,\"menuType\":\"C\",\"perms\":\"fm:custinfo:list\",\"status\":\"0\"}','{\"msg\":\"修改菜单\'用户信息\'失败，地址必须以http(s)://开头\",\"code\":500}',0,NULL,'2021-09-19 16:57:05'),(162,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"query\":\"\",\"icon\":\"guide\",\"orderNum\":\"4\",\"menuName\":\"墓园业务\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"fm\",\"children\":[],\"createTime\":1631376094000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-19 16:57:16'),(163,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"格位\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"cell\",\"component\":\"fm/cell/index\",\"children\":[],\"createTime\":1631801011000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2132,\"menuType\":\"C\",\"perms\":\"fm:cell:list\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-19 16:58:19'),(164,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin',NULL,'/system/menu','127.0.0.1','内网IP','{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"格位销售\",\"params\":{},\"parentId\":4,\"isCache\":\"0\",\"path\":\"cellOrder\",\"component\":\"fm/cellOrder/index\",\"children\":[],\"createTime\":1631801015000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2138,\"menuType\":\"C\",\"perms\":\"fm:cellOrder:list\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-19 16:58:29'),(165,'用户信息',1,'com.ruoyi.fm.controller.FmCustinfoController.add()','POST',1,'admin',NULL,'/fm/custinfo','127.0.0.1','内网IP','{\"sex\":\"0\",\"custName\":\"aaa\",\"params\":{},\"photone\":\"123123\",\"number\":\"121321\",\"id\":1}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-19 17:04:02'),(166,'逝者信息',1,'com.ruoyi.fm.controller.FmDeadController.add()','POST',1,'admin',NULL,'/fm/dead','127.0.0.1','内网IP','{\"deceased\":\"bb\",\"sex\":0,\"params\":{},\"number\":\"123123123\",\"id\":1,\"deceasedDate\":1631980800000}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-19 17:04:28'),(167,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_good_type\",\"dictLabel\":\" 商品\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:10:10'),(168,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_good_type\",\"dictLabel\":\"服务\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:10:28'),(169,'商品',1,'com.ruoyi.fm.controller.FmGoodsController.add()','POST',1,'admin',NULL,'/fm/goods','127.0.0.1','内网IP','{\"goodType\":1,\"params\":{},\"price\":10,\"name\":\"花\",\"id\":1}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:12:29'),(170,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_goods','127.0.0.1','内网IP','{tableName=fm_goods}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:15:42'),(171,'工程管理',1,'com.ruoyi.fm.controller.FmPrjController.add()','POST',1,'admin',NULL,'/fm/prj','127.0.0.1','内网IP','{\"createTime\":1632467839638,\"remark\":\"fasdfadf\",\"endTime\":1632326400000,\"params\":{},\"operator\":\"dfgdfg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmPrjMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmPrjMapper.insertFmPrj-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_prj          ( create_time,             remark,             end_time,             operator,             status )           values ( ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\n; Field \'login_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'login_id\' doesn\'t have a default value','2021-09-24 15:17:19'),(172,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_prj','127.0.0.1','内网IP','{tableName=fm_prj}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:18:14'),(173,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":223,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631800656000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":224,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"登录ID\",\"updateTime\":1631800656000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateBy\",\"usableColumn\":false,\"columnId\":225,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"updateTime\":1631800656000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":29,\"pk\":false,\"columnName\":\"create_by\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":226,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1631800656000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"q','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:20:30'),(174,'工程管理',1,'com.ruoyi.fm.controller.FmPrjController.add()','POST',1,'admin',NULL,'/fm/prj','127.0.0.1','内网IP','{\"createTime\":1632468061287,\"remark\":\"sdf\",\"endTime\":1632412800000,\"params\":{},\"operator\":\"sdfds\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmPrjMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmPrjMapper.insertFmPrj-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_prj          ( create_time,             remark,             end_time,             operator,             status )           values ( ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\n; Field \'login_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'login_id\' doesn\'t have a default value','2021-09-24 15:21:01'),(175,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":223,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632468030000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":224,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"登录ID\",\"updateTime\":1632468030000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":29,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateBy\",\"usableColumn\":false,\"columnId\":225,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"createBy\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建人\",\"isQuery\":\"1\",\"updateTime\":1632468030000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":29,\"pk\":false,\"columnName\":\"create_by\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":226,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":true,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1632468030000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"q','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:23:16'),(176,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631724293000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1631724293000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"updateTime\":1631724293000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"updateTime\":1631724293000,\"sort\":4,\"list\":true,\"params','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:29:45'),(177,'字典类型',1,'com.ruoyi.web.controller.system.SysDictTypeController.add()','POST',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"dictName\":\"fm_cell_type\",\"params\":{},\"dictType\":\"格位类型\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:34:34'),(178,'字典类型',2,'com.ruoyi.web.controller.system.SysDictTypeController.edit()','PUT',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"createTime\":1632468874000,\"updateBy\":\"admin\",\"dictName\":\"格位类型\",\"dictId\":102,\"params\":{},\"dictType\":\"fm_cell_type\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:34:57'),(179,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_cell_type\",\"dictLabel\":\"格位\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:35:58'),(180,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_cell_type\",\"dictLabel\":\"墓地\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:36:09'),(181,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632468584000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1632468584000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"updateTime\":1632468584000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"updateTime\":1632468584000,\"sort\":4,\"list\":true,\"params','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:39:52'),(182,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":169,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632469191000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(36)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435973000,\"tableId\":21,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":170,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1632469191000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Location\",\"usableColumn\":false,\"columnId\":171,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"location\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"位置\",\"isQuery\":\"1\",\"updateTime\":1632469191000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435973000,\"isEdit\":\"1\",\"tableId\":21,\"pk\":false,\"columnName\":\"location\"},{\"capJavaField\":\"Price\",\"usableColumn\":false,\"columnId\":172,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"price\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"价格\",\"updateTime\":1632469191000,\"sort\":4,\"list\":true,\"params','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:40:22'),(183,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"goodType\":1,\"params\":{},\"price\":24.0,\"fmGoodSellOrderList\":[],\"name\":\"ewrewr\",\"location\":\"werwer\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,                          good_type,             status )           values ( ?,             ?,             ?,                          ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 15:41:45'),(184,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631721751000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1631721751000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户ID\",\"isQuery\":\"1\",\"updateTime\":1631721751000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"CustomerName\",\"usableColumn\":false,\"columnId\":178,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"customerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户\",\"isQuery\":\"1\",\"updateTime\":1631721751000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:53:04'),(185,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_cell_sell_order','127.0.0.1','内网IP','{tableName=fm_cell_sell_order}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:53:10'),(186,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_cell_sell_order','127.0.0.1','内网IP','{tableName=fm_cell_sell_order}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:57:07'),(187,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632469983000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1632469983000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户ID\",\"updateTime\":1632469983000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"SalemanId\",\"usableColumn\":false,\"columnId\":245,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"salemanId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"销售人员ID\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"c','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:58:05'),(188,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632470284000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1632470284000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户ID\",\"updateTime\":1632470284000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"SalemanId\",\"usableColumn\":false,\"columnId\":245,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"salemanId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"销售人员ID\",\"updateTime\":1632470284000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Lon','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 15:59:50'),(189,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632470390000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1632470390000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户ID\",\"updateTime\":1632470390000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"SalemanId\",\"usableColumn\":false,\"columnId\":245,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"salemanId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"销售人员ID\",\"updateTime\":1632470390000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Lon','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:01:16'),(190,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_cell_sell_order','127.0.0.1','内网IP','{tableName=fm_cell_sell_order}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:01:23'),(191,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_cell_sell_order','127.0.0.1','内网IP','{tableName=fm_cell_sell_order}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:02:03'),(192,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632470476000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1632470476000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户ID\",\"updateTime\":1632470476000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"SalemanId\",\"usableColumn\":false,\"columnId\":245,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"salemanId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"销售人员ID\",\"updateTime\":1632470476000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Lon','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:03:36'),(193,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":175,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632470616000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":176,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1632470616000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CustomerId\",\"usableColumn\":false,\"columnId\":177,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"customerId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"用户ID\",\"updateTime\":1632470616000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":22,\"pk\":false,\"columnName\":\"customer_id\"},{\"capJavaField\":\"SalemanId\",\"usableColumn\":false,\"columnId\":245,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"salemanId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"销售人员ID\",\"updateTime\":1632470616000,\"sort\":3,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"i','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:07:04'),(194,'参数管理',2,'com.ruoyi.web.controller.system.SysConfigController.edit()','PUT',1,'admin',NULL,'/system/config','127.0.0.1','内网IP','{\"configName\":\"账号自助-验证码开关\",\"configKey\":\"sys.account.captchaOnOff\",\"createBy\":\"admin\",\"createTime\":1631376099000,\"updateBy\":\"admin\",\"configId\":4,\"remark\":\"是否开启验证码功能（true开启，false关闭）\",\"configType\":\"N\",\"configValue\":\"true\",\"params\":{}}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:08:50'),(195,'代码生成',2,'com.ruoyi.generator.controller.GenController.synchDb()','GET',1,'admin',NULL,'/tool/gen/synchDb/fm_good_sell_order','127.0.0.1','内网IP','{tableName=fm_good_sell_order}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 16:16:08'),(196,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":200,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1631721772000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":201,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1631721772000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":202,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1631721772000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"BETWEEN\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"create_time\"},{\"capJavaField\":\"Serial\",\"usableColumn\":false,\"columnId\":247,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"serial\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"订单编号\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(32)\",\"createBy\":\"\",\"isPk\":\"0\",\"cr','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:07:25'),(197,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":true,\"subTableName\":\"fm_order_goods_detail\",\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":200,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"updateTime\":1632474445000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":201,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"创建人\",\"updateTime\":1632474445000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"CreateTime\",\"usableColumn\":false,\"columnId\":202,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":true,\"updateBy\":\"\",\"javaField\":\"createTime\",\"htmlType\":\"datetime\",\"edit\":false,\"query\":true,\"columnComment\":\"创建日期\",\"isQuery\":\"1\",\"updateTime\":1632474445000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"BETWEEN\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":25,\"pk\":false,\"columnName\":\"create_time\"},{\"capJavaField\":\"Serial\",\"usableColumn\":false,\"columnId\":247,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isRequired\":\"1\",\"javaField\":\"serial\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"columnComment\":\"订单编号\",\"isQuery\":\"1\",\"updateTime\":1632474445000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryT','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:08:37'),(198,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":231,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1631800666000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":232,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"登录id\",\"updateTime\":1631800666000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"OperatorName\",\"usableColumn\":false,\"columnId\":233,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人姓名\",\"isQuery\":\"1\",\"updateTime\":1631800666000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"operator_name\"},{\"capJavaField\":\"OperatorIdentification\",\"usableColumn\":false,\"columnId\":234,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorIdentification\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人身份证\",\"isQuery\":\"1\",\"updateTime\":1631800666000,','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:14:03'),(199,'字典类型',1,'com.ruoyi.web.controller.system.SysDictTypeController.add()','POST',1,'admin',NULL,'/system/dict/type','127.0.0.1','内网IP','{\"createBy\":\"admin\",\"dictName\":\"格位操作\",\"params\":{},\"dictType\":\"fm_cell_op\",\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:15:09'),(200,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_cell_op\",\"dictLabel\":\"迁入\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:15:31'),(201,'字典数据',1,'com.ruoyi.web.controller.system.SysDictDataController.add()','POST',1,'admin',NULL,'/system/dict/data','127.0.0.1','内网IP','{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"fm_cell_op\",\"dictLabel\":\"迁出\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:15:41'),(202,'代码生成',2,'com.ruoyi.generator.controller.GenController.editSave()','PUT',1,'admin',NULL,'/tool/gen','127.0.0.1','内网IP','{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":231,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1632474843000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"LoginId\",\"usableColumn\":false,\"columnId\":232,\"isIncrement\":\"0\",\"increment\":false,\"insert\":false,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"javaField\":\"loginId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"登录id\",\"updateTime\":1632474843000,\"sort\":2,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(11)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"tableId\":30,\"pk\":false,\"columnName\":\"login_id\"},{\"capJavaField\":\"OperatorName\",\"usableColumn\":false,\"columnId\":233,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人姓名\",\"isQuery\":\"1\",\"updateTime\":1632474843000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(32)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1631435974000,\"isEdit\":\"1\",\"tableId\":30,\"pk\":false,\"columnName\":\"operator_name\"},{\"capJavaField\":\"OperatorIdentification\",\"usableColumn\":false,\"columnId\":234,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":true,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"isRequired\":\"1\",\"javaField\":\"operatorIdentification\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"办理人身份证\",\"isQuery\":\"1\",\"updateTime\":1632474843000,','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 17:16:43'),(203,'代码生成',8,'com.ruoyi.generator.controller.GenController.batchGenCode()','GET',1,'admin',NULL,'/tool/gen/batchGenCode','127.0.0.1','内网IP','{}','null',0,NULL,'2021-09-24 17:27:33'),(204,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:04'),(205,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:09'),(206,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:13'),(207,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:17'),(208,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:21'),(209,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:25'),(210,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":25.0,\"name\":\"sdfsd\",\"location\":\"dfgdffg\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmCellMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmCellMapper.insertFmCell-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_cell          ( name,             location,             price,             type,                          status )           values ( ?,             ?,             ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value','2021-09-24 18:14:29'),(211,'格位',1,'com.ruoyi.fm.controller.FmCellController.add()','POST',1,'admin',NULL,'/fm/cell','127.0.0.1','内网IP','{\"params\":{},\"type\":1,\"price\":23.0,\"name\":\"dsf\",\"location\":\"fghfgh\",\"status\":0}','{\"msg\":\"操作成功\",\"code\":200}',0,NULL,'2021-09-24 18:16:33'),(212,'工程管理',1,'com.ruoyi.fm.controller.FmPrjController.add()','POST',1,'admin',NULL,'/fm/prj','127.0.0.1','内网IP','{\"createTime\":1632495313991,\"remark\":\"888\",\"params\":{},\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmPrjMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmPrjMapper.insertFmPrj-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_prj          ( create_time,             remark,                                       status )           values ( ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\n; Field \'login_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'login_id\' doesn\'t have a default value','2021-09-24 22:55:14'),(213,'工程管理',1,'com.ruoyi.fm.controller.FmPrjController.add()','POST',1,'admin',NULL,'/fm/prj','127.0.0.1','内网IP','{\"createTime\":1632498108581,\"remark\":\"fssd\",\"params\":{},\"operator\":\"sdfsd\",\"status\":0}','null',1,'\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\r\n### The error may exist in file [F:\\学习工作\\工作\\开发\\project2\\ruoyi-vue\\rouyi-fm\\target\\classes\\mapper\\fm\\FmPrjMapper.xml]\r\n### The error may involve com.ruoyi.fm.mapper.FmPrjMapper.insertFmPrj-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into fm_prj          ( create_time,             remark,                          operator,             status )           values ( ?,             ?,                          ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'login_id\' doesn\'t have a default value\n; Field \'login_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'login_id\' doesn\'t have a default value','2021-09-24 23:41:48');
/*!40000 ALTER TABLE `sys_oper_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_post`
--

DROP TABLE IF EXISTS `sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='岗位信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_post`
--

LOCK TABLES `sys_post` WRITE;
/*!40000 ALTER TABLE `sys_post` DISABLE KEYS */;
INSERT INTO `sys_post` VALUES (1,'ceo','董事长',1,'0','admin','2021-09-12 00:01:34','',NULL,''),(2,'se','项目经理',2,'0','admin','2021-09-12 00:01:34','',NULL,''),(3,'hr','人力资源',3,'0','admin','2021-09-12 00:01:34','',NULL,''),(4,'user','普通员工',4,'0','admin','2021-09-12 00:01:34','',NULL,'');
/*!40000 ALTER TABLE `sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'超级管理员','admin',1,'1',1,1,'0','0','admin','2021-09-12 00:01:34','',NULL,'超级管理员'),(2,'普通角色','common',2,'2',1,1,'0','0','admin','2021-09-12 00:01:34','admin','2021-09-12 18:08:59','普通角色');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_dept`
--

DROP TABLE IF EXISTS `sys_role_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色和部门关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_dept`
--

LOCK TABLES `sys_role_dept` WRITE;
/*!40000 ALTER TABLE `sys_role_dept` DISABLE KEYS */;
INSERT INTO `sys_role_dept` VALUES (2,100),(2,101),(2,105);
/*!40000 ALTER TABLE `sys_role_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色和菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (2,1),(2,2),(2,3),(2,4),(2,100),(2,101),(2,102),(2,103),(2,104),(2,105),(2,106),(2,107),(2,108),(2,109),(2,110),(2,111),(2,112),(2,113),(2,114),(2,115),(2,116),(2,500),(2,501),(2,1001),(2,1002),(2,1003),(2,1004),(2,1005),(2,1006),(2,1007),(2,1008),(2,1009),(2,1010),(2,1011),(2,1012),(2,1013),(2,1014),(2,1015),(2,1016),(2,1017),(2,1018),(2,1019),(2,1020),(2,1021),(2,1022),(2,1023),(2,1024),(2,1025),(2,1026),(2,1027),(2,1028),(2,1029),(2,1030),(2,1031),(2,1032),(2,1033),(2,1034),(2,1035),(2,1036),(2,1037),(2,1038),(2,1039),(2,1040),(2,1041),(2,1042),(2,1043),(2,1044),(2,1045),(2,1046),(2,1047),(2,1048),(2,1049),(2,1050),(2,1051),(2,1052),(2,1053),(2,1054),(2,1055),(2,1056),(2,1057),(2,1058),(2,1059),(2,1060),(2,2000),(2,2001),(2,2002),(2,2003),(2,2004),(2,2005),(2,2006),(2,2007),(2,2008),(2,2009),(2,2010),(2,2011),(2,2012),(2,2013),(2,2014),(2,2015),(2,2016),(2,2017),(2,2018),(2,2019),(2,2020),(2,2021),(2,2022),(2,2023),(2,2024),(2,2025),(2,2026),(2,2027),(2,2028),(2,2029),(2,2030),(2,2031),(2,2032),(2,2033),(2,2034),(2,2035),(2,2036),(2,2037),(2,2038),(2,2039),(2,2040),(2,2041);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT '' COMMENT '密码',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,103,'admin','若依','00','ry@163.com','15888888888','1','','$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2','0','0','127.0.0.1','2021-09-25 11:15:16','admin','2021-09-12 00:01:34','','2021-09-25 11:15:15','管理员'),(2,105,'ry','若依','00','ry@qq.com','15666666666','1','','$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2','0','0','127.0.0.1','2021-09-12 00:01:34','admin','2021-09-12 00:01:34','',NULL,'测试员');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_post`
--

DROP TABLE IF EXISTS `sys_user_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户与岗位关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_post`
--

LOCK TABLES `sys_user_post` WRITE;
/*!40000 ALTER TABLE `sys_user_post` DISABLE KEYS */;
INSERT INTO `sys_user_post` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `sys_user_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户和角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-25 12:24:09
